# Indrek Synter 2013

from PySide import QtGui, QtCore, QtUiTools

import os
import logging
import traceback

import ctypes
import numpy, scipy
import math

# TODO:: Use more numpy

import main_global as mg

"""
A generic abstract simulation class
"""
class ESim(QtCore.QThread):
	iteration_done = QtCore.Signal()
	algorithm_done = QtCore.Signal(QtGui.QImage)
	
	CS_CARTESIAN_2D = 1
	CS_CYLINDRICAL = 2

	def __init__(self):
		super(ESim, self).__init__()
		
		self.log = logging.getLogger(__name__)
		
		self.use_cmath = True
		
		self.coordsys = self.CS_CARTESIAN_2D
		self.w = 1000
		self.h = 1000
		self.scale = 1.0
		self.alpha = None
		
		self.algorithm = 0

		self.num_iterations_so_far = 0
		self.num_iterations = 1000000
		self.error_so_far = 10000
		self.error_tolerance = 0
		
		self.image = None
		
		self.update_interval = 10
		self.t = QtCore.QThread()
		self.moveToThread(self.t)
		self.updateTimer = QtCore.QTimer()
		self.updateTimer.timeout.connect(self.update)
		self.updateTimer.setInterval(self.update_interval)
		self.updateTimer.moveToThread(self.t)
		self.t.started.connect(self.updateTimer.start)
		self.t.finished.connect(self.updateTimer.stop)
		
		self.running = False
	
	# Create a base for the simulation
	def create(self, w, h, scale):
		self.w = w
		self.h = h
		self.scale = scale

		self.image = QtGui.QImage(self.w, self.h, QtGui.QImage.Format_Indexed8)
		self.image.setColorCount(255)
		
		self.log.info("Simulation base created")
		
		self.libsim_math = None
		self.load_clib()

	def load_clib(self):
		path = ""
		if os.path.exists("./sim_math/sim_math.so"):
			path = "./sim_math/sim_math.so"
		elif os.path.exists("./sim_math/sim_math.dll"):
			path = "./sim_math/sim_math.dll"
		else:
			self.log.info("Could not find C library for simulation math")
			self.log.info("Falling back to python math")
			self.use_cmath = False
			return
	
		self.log.info("Loading C library for simulation math: {}".format(path))
		
		try:
			self.libsim_math = ctypes.cdll.LoadLibrary(path)
			self.libsim_math.fgauss_seidel.restype = ctypes.c_float
		except Exception as e:
			self.use_cmath = False
			self.log.exception(e)
			self.log.info("Falling back to python math")
		
	def width(self):
		return self.w
	
	def height(self):
		return self.h

	def draw_rect(self, x0, y0, x1, y1, potential):
		pass
	
	def draw_ellipse(self, x0, y0, x1, y1, potential):
		pass
	
	def create_point_charge(self, x, y, potential):
		pass

	def create_line_charge(self, x0, y0, x1, y1, potential):
		pass
	
	def create_circle_charge(self, x, y, w, h, potential):
		pass

	def create_control_point(self, x, y, potential):
		pass
	
	def set_num_iterations(self, num_iterations):
		self.num_iterations = num_iterations
	
	def set_error_tolerance(self, tolerance):
		self.error_tolerance = tolerance
		
	def set_algorithm(self, algorithm):
		self.algorithm = algorithm
	
	def set_coordsys(self, coordsys):
		if coordsys < 0:
			coordsys = CS_CARTESIAN_2D
		self.coordsys = coordsys
		
	def set_scale(self, scale):
		self.scale = scale
	
	def start(self):
		self.running = True
		self.t.start()
	
	def init_algorithm(self):
		pass

	def run_algorithm(self):
		pass
	
	def end_algorithm(self):
		self.algorithm_done.emit(self.image)
	
	def update(self):
		if self.running:
			if self.num_iterations_so_far == 0:
				self.init_algorithm()
				self.log.info("Starting the simulation")

			self.run_algorithm()
			
			self.num_iterations_so_far += 1
			self.iteration_done.emit()
			
			if self.num_iterations_so_far >= self.num_iterations:
				self.log.info("Finished {} iterations, stopping".format(self.num_iterations))
				self.running = False
				self.end_algorithm()
			if self.error_so_far <= self.error_tolerance:
				self.log.info("Reached desired error: {}, stopping".format(self.error_tolerance))
				self.running = False
				self.end_algorithm()
	
	def stop(self):
		self.log.info("Stopping the simulation")
		self.running = False
		self.t.quit()
		#while not self.t.isFinished():
		#	pass

	# Convert it into an image
	def to_image(self, output_fname, data, min_value, max_value):
		self.log.info("Converting to image")
		
		for i in range(0, 256):
			self.image.setColor(i, QtGui.qRgb(i, i, i))
		
		w = self.w
		for y in range(0, self.h):
			for x in range(0, self.w):
				if max_value - min_value != 0:
					# Normalize the values
					normalized = (data[x + y * w] - min_value) / (max_value - min_value)
				else:
					normalized = 0
				if math.isnan(normalized):
					normalized = 0
				elif math.isinf(normalized):
					normalized = 1
				# And convert it into an 8-bit value
				value = int(round(255 * normalized)) & 0xFF
				# Put a pixel onto the image
				self.image.setPixel(x, y, value)
		
		self.log.info("Saving image")
		self.image.save(output_fname)
	
	def get_image(self):
		return self.image
		
	def get_iteration_count(self):
		return self.num_iterations_so_far
	
	def get_error_margin(self):
		return self.error_so_far
	
	def get_value(self, x, y):
		if x < 0 or x >= self.image.width() or y < 0 or y >= self.image.height():
			return 0
		return QtGui.qGray(self.image.pixel(x, y))
	
	# Saves a matrix to CSV
	def save_m_csv(self, filename, matrix, ncols=1):
		nrows = len(matrix) / ncols
		
		with open(filename, "wt") as f:
			for i in range(0, nrows):
				line = ""
				for j in range(0, ncols):
					line += "{:f},".format(matrix[j + i * ncols])
				f.write(line + "\n")
	
	def print_m(self, matrix, ncols=1):
		nrows = len(matrix) / ncols
		
		for i in range(0, nrows):
			line = ""
			for j in range(0, ncols):
				line += "{:f}, ".format(matrix[j + i * ncols])
			print line
	
	def save_sim(self, f):
		f.write('sim.image_width = {}\nsim.image_height = {}\n'.format(self.w, self.h))
