# -*- mode: python -*-
a = Analysis(['elm\\es.py'],
             pathex=['C:\\ELM'],
             hiddenimports=[],
             hookspath=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=1,
          name=os.path.join('build\\pyi.win32\\elm', 'elm.exe'),
          debug=False,
          strip=None,
          upx=True,
          console=True)
a.datas += [('ui\\main.ui', 'elm\\ui\\main.ui', 'DATA')]
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=True,
               name=os.path.join('dist', 'elm'))
