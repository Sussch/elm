from PySide import QtGui, QtCore

from simulation import ESim

import ctypes
import numpy, scipy
import math

"""
Fixed distances between elements.
"""
class ESimFD(ESim):
	M_GAUSS_SEIDEL = 0
	M_OVER_RELAXATION = 1
	
	MF_FIXED = 0x80000000
	
	Algorithms = ["Gauss Seidel", "Over-relaxation"]
	
	def __init__(self):
		ESim.__init__(self)
		
		self.potential = None
		self.efield = None
		self.efield_valid = False
		self.metadata = None
		self.edensity = 10.0
		
		self.min_value = 0
		self.max_value = 0
		self.min_efvalue = 0
		self.max_efvalue = 0
		
		self.point_electrodes = []
		self.rect_electrodes = []
		self.ellipse_electrodes = []
		self.poly_electrodes = []
		
		self.algorithm = self.M_GAUSS_SEIDEL
	
	# Create a base for the simulation
	def create(self, w, h, scale):
		ESim.create(self, w, h, scale)
		
		# Note that in Python, 1D numpy arrays are a lot faster than 2D numpy arrays
		self.potential = (ctypes.c_float * (self.w * self.h))(0)
		self.efield = (ctypes.c_float * (self.w * self.h))(0)
		self.metadata = (ctypes.c_uint * (self.w * self.h))(0)
		
		self.log.info("FDM Simulation field created")
		
	def set_algorithm(self, algorithm):
		self.algorithm = algorithm
		
		if algorithm == self.M_GAUSS_SEIDEL:
			self.alpha = 1.0
			
	def draw_point(self, x, y, potential):
		self.potential[x + y * self.w] = potential
		self.metadata[x + y * self.w] |= self.MF_FIXED
	
	@QtCore.Slot(int, int, int, int, float)
	# Draw a rectangle on the simulation space
	def draw_rect(self, x0, y0, x1, y1, potential):
		if self.potential == None or self.metadata == None:
			return
		
		if potential < self.min_value:
			self.min_value = potential
		if potential > self.max_value:
			self.max_value = potential
		
		for y in range(y0, y1):
			for x in range(x0, x1):
				self.draw_point(x, y, potential)
		
		self.rect_electrodes.append([x0, y0, x1, y1, potential])
		self.log.debug("FDM: Rect({}, {}, {}, {}, {} V)".format(x0, y0, x1, y1, potential))
	
	@QtCore.Slot(int, int, int, int, float)
	# Draw an ellipse on the simulation space
	def draw_ellipse(self, x0, y0, x1, y1, potential):
		if self.potential == None or self.metadata == None:
			return
		
		if potential < self.min_value:
			self.min_value = potential
		if potential > self.max_value:
			self.max_value = potential
		
		# http://stackoverflow.com/questions/10322341/simple-algorithm-for-drawing-filled-ellipse-in-c-c
		ox = (x0 + x1) / 2
		oy = (y0 + y1) / 2
		w = (x1 - x0) / 2
		h = (y1 - y0) / 2
		for y in range(-h, h + 1):
			for x in range(-w, w + 1):
				if (x * x * h * h + y * y * w * w <= h * h * w * w):
					self.draw_point(ox + x, oy + y, potential)
		
		self.ellipse_electrodes.append([x0, y0, x1, y1, potential])
		self.log.debug("FDM: Ellipse({}, {}, {}, {}, {} V)".format(x0, y0, x1, y1, potential))
	
	def init_algorithm(self):
		self.efield_valid = False
		self.log.info("FDM Algorithm initialized")

	def run_algorithm(self):
		if self.algorithm == self.M_GAUSS_SEIDEL or self.algorithm == self.M_OVER_RELAXATION:
			self.m_gauss_seidel()
	
	def end_algorithm(self):
		# Convert the results into an image
		self.output_potential()
		#ESim.end_algorithm(self)
		
	def m_gauss_seidel(self):
		if self.w == 0 or self.h == 0:
			return
		# No alpha? Calculate it
		if self.alpha == None:
			w = self.w - 2
			h = self.h - 2
			self.alpha = 2 - math.pi * math.sqrt(2.0 / (h * h) + 2.0 / (w * w))
		
		# Have a C simulation math lib? Use it
		if self.libsim_math != None and self.use_cmath:
			if self.coordsys == self.CS_CARTESIAN_2D:
				self.error_so_far = self.libsim_math.fgauss_seidel(self.potential, self.metadata, self.w, self.h, ctypes.c_float(self.alpha))
			elif self.coordsys == self.CS_CYLINDRICAL:
				self.error_so_far = self.libsim_math.fgauss_seidel_cyl(self.potential, self.metadata, self.w, self.h, ctypes.c_float(self.alpha))
		# No C simulation math lib? It'll be somewhat slower, then.
		else:
			max_diff = 0
			
			w = self.w
			# Cartesian coordinates on 2D plane
			if self.coordsys == self.CS_CARTESIAN_2D:
				for y in range(1, self.h - 1):
					for x in range(1, self.w - 1):
						if (self.metadata[x + y * w] & self.MF_FIXED) != 0:
							continue
						
						# TODO:: Use asymmetric formula near fixed edges
						
						diff = (self.potential[x - 1 + y * w] + self.potential[x + (y - 1) * w] + self.potential[x + 1 + y * w] + self.potential[x + (y + 1) * w]) / 4.0 - self.potential[x + y * w]					
						self.potential[x + y * w] += self.alpha * diff
						# Find the maximum difference between the old and the new values
						if max_diff < self.alpha * diff:
							max_diff = self.alpha * diff
			# Cylindrical coordinates, with an axis of symmetry at x == 0
			elif self.coordsys == self.CS_CYLINDRICAL:
				for y in range(1, self.h - 1):
					for x in range(1, self.w - 1):
						if (self.metadata[x + y * w] & self.MF_FIXED) != 0:
							continue
						
						# TODO:: Use asymmetric formula near fixed edges
						
						# On the axis of symmetry?
						if x == 0:
							diff = (self.potential[x + (y - 1) * w] + 4 * self.potential[x + 1 + y * w] + self.potential[x + (y + 1) * w]) / 6.0 - self.potential[x + y * w]
						else:
							diff = ((1 - 1 / (2 * x)) * self.potential[x - 1 + y * w] + self.potential[x + (y - 1) * w] + (1 + 1 / (2 * x)) * self.potential[x + 1 + y * w] + self.potential[x + (y + 1) * w]) / 4.0 - self.potential[x + y * w]
						
						self.potential[x + y * w] += self.alpha * diff
						# Find the maximum difference between the old and the new values
						if max_diff < self.alpha * diff:
							max_diff = self.alpha * diff

			# Use the maximum difference as an estimate for error
			self.error_so_far = max_diff
			
	def m_efield(self):
		self.log.info("Calculating electric field")
		
		if self.libsim_math != None and self.use_cmath:
			self.libsim_math.felectric_field(self.potential, self.efield, self.w, self.h, ctypes.c_float(self.edensity), ctypes.c_bool(self.coordsys == self.CS_CYLINDRICAL))
		else:
			w = self.w
			h = self.h
			for y in range(0, self.h):
				for x in range(0, self.w):
					if x > 0 and x < w -1 and y > 0 and y < h - 1:
						dx = (self.potential[x + 1 + y * w] - self.potential[x - 1 + y * w]) * self.edensity / 2.0
						dy = (self.potential[x + (y + 1) * w] - self.potential[x + (y - 1) * w]) * self.edensity / 2.0
					else:
						# Single-sided derivative
						if x == 0:
							if self.coordsys == self.CS_CYLINDRICAL:
								dx = abs(self.potential[x + (y + 1) * w] - self.potential[x + (y - 1) * w]) * self.edensity / 2.0
							else:
								dx = (self.potential[x + 1 + y * w] - self.potential[x + y * w]) * self.edensity
						elif x == w - 1:
							dx = (self.potential[x + y * w] - self.potential[x - 1 + y * w]) * self.edensity
						if y == 0:
							dy = (self.potential[x + (y + 1) * w] - self.potential[x + y * w]) * self.edensity
						elif y == h - 1:
							dy = (self.potential[x + y * w] - self.potential[x + (y - 1) * w]) * self.edensity
					
					self.efield[x + y * w] = math.sqrt(dx * dx + dy * dy)
		
		# Find minimum and maximum values
		self.min_efvalue = min(self.efield)
		self.max_efvalue = max(self.efield)
		
		self.efield_valid = True
	
	@QtCore.Slot()
	def output_efield(self):
		if not self.efield_valid:
			self.m_efield()
		
		self.log.info("Converting electric field to an image")
		
		# Convert it to an image and emit a "done" signal to display it
		self.to_image("efield.png", self.efield, self.min_efvalue, self.max_efvalue)
		ESim.end_algorithm(self)
	
	@QtCore.Slot()
	def output_potential(self):
		self.log.info("Converting potential field to an image")
		
		# Convert it to an image and emit a "done" signal to display it
		self.to_image("potential.png", self.potential, self.min_value, self.max_value)
		ESim.end_algorithm(self)
	
	def save_sim(self, f):
		ESim.save_sim(self, f)
		f.write('sim.edensity = {}\n'.format(self.edensity))
	
	def get_value(self, x, y):
		potential = ESim.get_value(self, x, y) * (self.max_value - self.min_value) / 255.0 + self.min_value
		return "{} V".format(potential)

	