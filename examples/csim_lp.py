init_sim(2)
sim.image_width = 600
sim.image_height = 600
sim.edensity = 10.0
sim.epsilon0 = 8.854e-12

set_coordsys(0)
set_algorithm(0)
set_num_iterations(1)
set_error_tolerance(-3)
set_sim_scale(1.0, 0)
set_grid(0.02, True, True)

scene.charge_line(60, 20, 70, 80, 100.0)
scene.charge_point(13, 82, 100.0)
scene.control_point(34, 85, 100.0)
scene.control_point(49, 53, 100.0)
