init_sim(3)
sim.image_width = 600
sim.image_height = 600
sim.edensity = 10.0
sim.epsilon0 = 8.854e-12

set_coordsys(0)
set_algorithm(0)
set_num_iterations(1)
set_error_tolerance(-3)
set_sim_scale(1.0, 0)
set_grid(0.02, True, True)

scene.electrode_rect(80, 150, 50, 110, 100.0)
scene.electrode_rect(130, 190, 50, 30, 100.0)
scene.electrode_rect(180, 150, 50, 110, 100.0)
