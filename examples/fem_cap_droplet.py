init_sim(1)
sim.image_width = 600
sim.image_height = 600
sim.ew = 16
sim.eh = 16
sim.edensity = 10.0

set_coordsys(0)
set_algorithm(3)
set_num_iterations(20)
set_error_tolerance(-3)
set_sim_scale(1.0, 0)
set_grid(0.02, True, True)

scene.electrode_rect(20, 20, 570, 40, 100.0)
scene.electrode_rect(20, 550, 570, 40, -100.0)
scene.electrode_ellipse(340, 240, 160, 140, -0.0)
