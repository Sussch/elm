init_sim(3)
sim.image_width = 600
sim.image_height = 600
sim.edensity = 10.0
sim.epsilon0 = 8.854e-12

set_coordsys(0)
set_algorithm(0)
set_num_iterations(1)
set_error_tolerance(-3)
set_sim_scale(1.0, 0)
set_grid(0.02, True, True)

scene.electrode_rect(10, 270, 580, 320, 1.0)
