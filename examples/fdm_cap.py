init_sim(0)
sim.image_width = 600
sim.image_height = 600
sim.edensity = 10.0

set_coordsys(0)
set_algorithm(1)
set_num_iterations(600)
set_error_tolerance(-3)
set_sim_scale(1.0, 0)
set_grid(0.02, True, True)

scene.electrode_rect(12, 552, 576, 36, -100.0)
scene.electrode_rect(12, 12, 576, 24, 100.0)
