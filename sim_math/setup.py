#!/usr/bin/python
# Thanks to https://gist.github.com/beaylott/3313315

import os
 
from distutils.core import setup
from distutils import ccompiler, msvccompiler
import distutils.log

distutils.log.set_verbosity(-1)
distutils.log.set_verbosity(distutils.log.DEBUG)

def build_ctypes_ext(name,sources):
	obj_ext = '.o'
	dll_ext = '.so'

	compiler = ccompiler.new_compiler()
	
	if isinstance(compiler, msvccompiler.MSVCCompiler):
		obj_ext = '.obj'
		dll_ext = '.dll'
		compiler.compile(sources)
	else:
		compiler.compile(sources, extra_preargs=["-fPIC"])

	def get_onames(sources, obj_ext):
		o_names = []
		for source in sources:
			(head, tail) = os.path.split(source)
			o_name = os.path.splitext(tail)[0] + obj_ext
			o_names.append(os.path.join(head, o_name))

		return o_names

	compiler.link_shared_object(get_onames(sources, obj_ext), name + dll_ext, libraries=["m","c"])

build_ctypes_ext("sim_math", ["sim_math.cpp"])
 
setup()
