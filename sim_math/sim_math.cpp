// Indrek Synter 2013
// Thanks to https://gist.github.com/dfm/3343607

#include <stdio.h>
#include <math.h>

#define M_C(x, y, w)	((y) * (w) + (x))

#ifdef _MSC_VER
#define DLLEXPORT   __declspec(dllexport)
#else
#define DLLEXPORT
#endif

typedef enum meta_te {
	MF_FIXED = 0x80000000,	///< Is this a conductor with fixed potential?
	MF_HN_MASK = 0x0000000F,	///< Distance to fixed surface in the North
	MF_HS_MASK = 0x000000F0,	///< Distance to fixed surface in the South
	MF_HW_MASK = 0x00000F00,	///< Distance to fixed surface in the West
	MF_HE_MASK = 0x0000F000,	///< Distance to fixed surface in the East
	MF_HF_MASK = 0x000F0000,	///< Distance to fixed surface at the Front
	MF_HB_MASK = 0x00F00000,	///< Distance to fixed surface at the Back
	MF_HX_MASK = 0x00FFFFFF		///< Mask to check if we need to use asymmetric formulas
} meta_t;

extern "C" DLLEXPORT float fgauss_seidel(float *potential, unsigned int *metadata, int w, int h, float alpha) {
	float max_diff = 0.0f, diff = 0.0f;
	int x, y;
	
	for (y=1; y<h-1; y++) {
		for (x=1; x<w-1; x++) {
			// Skip, in case this point already has a fixed potential
			if ((metadata[M_C(x,y,w)] & MF_FIXED) != 0)
				continue;
			//! \todo Use asymmetric formula near fixed edges
			diff = (potential[M_C(x-1,y,w)] + potential[M_C(x,y-1,w)] + potential[M_C(x+1,y,w)] + potential[M_C(x,y+1,w)]) / 4.0f - potential[M_C(x,y,w)];
			diff *= alpha;
			potential[M_C(x,y,w)] += diff;
			// Find the maximum difference between the old and the new values
			if (max_diff < diff)
				max_diff = diff;
		}
	}
	return max_diff;
}

extern "C" DLLEXPORT float fgauss_seidel_cyl(float *potential, unsigned int *metadata, int w, int h, float alpha) {
	float max_diff = 0.0f, diff = 0.0f;
	int x, y;
	
	for (y=1; y<h-1; y++) {
		for (x=0; x<w-1; x++) {
			// Skip, in case this point already has a fixed potential
			if ((metadata[M_C(x,y,w)] & MF_FIXED) != 0)
				continue;
			//! \todo Support for a custom axis of symmetry
			//! \todo Use asymmetric formula near fixed edges

			// We're on the axis of symmetry?
			if (x == 0) {
				diff = (potential[M_C(x,y-1,w)] + 4*potential[M_C(x+1,y,w)] + potential[M_C(x,y+1,w)]) / 6.0f - potential[M_C(x,y,w)];
			} else {
				diff = ((1-1/(2*x))*potential[M_C(x-1,y,w)] + potential[M_C(x,y-1,w)] + (1+1/(2*x))*potential[M_C(x+1,y,w)] + potential[M_C(x,y+1,w)]) / 4.0f - potential[M_C(x,y,w)];
			}
			diff *= alpha;
			potential[M_C(x,y,w)] += diff;
			// Find the maximum difference between the old and the new values
			if (max_diff < diff)
				max_diff = diff;
		}
	}
	return max_diff;
}

extern "C" DLLEXPORT void felectric_field(float *potential, float *electric_field, int w, int h, float density, bool cylindrical) {
	float dx, dy;
	int x, y;
	
	for (y=0; y<h; y++) {
		for (x=0; x<w; x++) {
			if (x > 0 && x < w -1 && y > 0 && y < h - 1) {
				dx = (potential[M_C(x+1,y,w)] - potential[M_C(x-1,y,w)]) * density / 2.0f;
				dy = (potential[M_C(x,y+1,w)] - potential[M_C(x,y-1,w)]) * density / 2.0f;
			} else {
				// Single-sided derivative
				if (x == 0) {
					if (cylindrical)
						dx = fabs(potential[M_C(x,y+1,w)] - potential[M_C(x,y-1,w)]) * density / 2.0f;
					else
						dx = (potential[M_C(x+1,y,w)] - potential[M_C(x,y,w)]) * density;
				} else if (x == w - 1)
					dx = (potential[M_C(x,y,w)] - potential[M_C(x-1,y,w)]) * density;
				if (y == 0)
					dy = (potential[M_C(x,y+1,w)] - potential[M_C(x,y,w)]) * density;
				else if (y == h - 1)
					dy = (potential[M_C(x,y,w)] - potential[M_C(x,y-1,w)]) * density;
			}
			
			electric_field[M_C(x,y,w)] = sqrtf(dx * dx + dy * dy);
		}
	}
}
