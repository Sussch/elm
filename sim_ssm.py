from PySide import QtGui, QtCore

from simulation import ESim

import ctypes
import numpy, scipy, scipy.special
import math

"""
Charge simulation
"""
class ESimSC(ESim):
	Algorithms = ["Charge", "Potential"]
	
	ELEM_LINE = 1
	
	def __init__(self):
		ESim.__init__(self)
		
		self.potential = None
		self.efield = None
		self.efield_valid = False
		self.edensity = 10.0
		self.epsilon0 = 8.854e-12
		self.m_k = None
		self.v_u = None
		self.v_q = None
		
		self.e_patches = []
		self.pw = 10
		self.ph = 10
		
		self.min_value = 0
		self.max_value = 0
		self.min_cvalue = 0
		self.max_cvalue = 0
		self.min_efvalue = 0
		self.max_efvalue = 0
		
		self.rect_electrodes = []
		self.ellipse_electrodes = []
		self.poly_electrodes = []
		
		self.algorithm = 0
	
	# Create a base for the simulation
	def create(self, w, h, scale):
		ESim.create(self, w, h, scale)
		# Crashes on rect draw, when pw or ph are too small
		self.pw = self.w / 30
		self.ph = self.h / 30
		
		self.potential = (ctypes.c_float * (self.w * self.h))(0)
		self.efield = (ctypes.c_float * (self.w * self.h))(0)
		
		self.log.info("Surface Charge Simulation field created")
		
	@QtCore.Slot(int, int, int, int, float)
	# Draw a rectangle on the simulation space
	def draw_rect(self, x0, y0, x1, y1, potential):
		if potential < self.min_value:
			self.min_value = potential
		if potential > self.max_value:
			self.max_value = potential
		
		# TODO:: Divide it into rectangular elements
		for y in range(y0, y1, self.ph):
			for x in range(x0, x1, self.pw):
				self.e_patches.append([[x, y], potential])
		
		self.rect_electrodes.append([x0, y0, x1, y1, potential])
		self.log.debug("SC: Rect({}, {}, {}, {}, {} V)".format(x0, y0, x1, y1, potential))
	
	@QtCore.Slot(int, int, int, int, float)
	# Draw an ellipse on the simulation space
	def draw_ellipse(self, x0, y0, x1, y1, potential):
		if potential < self.min_value:
			self.min_value = potential
		if potential > self.max_value:
			self.max_value = potential
		
		# http://stackoverflow.com/questions/10322341/simple-algorithm-for-drawing-filled-ellipse-in-c-c
		ox = (x0 + x1) / 2
		oy = (y0 + y1) / 2
		w = (x1 - x0) / 2
		h = (y1 - y0) / 2
		for y in range(-h, h + 1):
			for x in range(-w, w + 1):
				if (x * x * h * h + y * y * w * w <= h * h * w * w):
					# TODO:: Sector elements?
					pass
					#self.draw_point(ox + x, oy + y, potential)
		
		self.ellipse_electrodes.append([x0, y0, x1, y1, potential])
		self.log.debug("SC: Ellipse({}, {}, {}, {}, {} V)".format(x0, y0, x1, y1, potential))
	
	def init_algorithm(self):
		self.efield_valid = False
		self.log.info("Surface charge simulation algorithm initialized")

	def run_algorithm(self):
		self.m_calculate()
	
	def end_algorithm(self):
		# Convert the results into an image
		self.output_potential()
		#ESim.end_algorithm(self)
	
	def p_patch_ns(self, x, y, a0, b0, a1, b1):
		c = (x - (a0 + a1) / 2)**2
		d = (y - (b0 + b1) / 2)**2
		
		return((a1 - a0) * (b1 - b0) / (4 * math.pi * self.epsilon0 * math.sqrt(c + d)))

	def p_patch_s(self, a0, b0, a1, b1):
		return(0.2806 * math.sqrt((a1 - a0) * (b1 - b0)) / self.epsilon0)
	
	def calc_coeff_m(self):
		Np = len(self.e_patches)
		
		if self.m_k == None:
			self.m_k = numpy.matrix(numpy.zeros(shape=(Np, Np), dtype='float32'))
		if self.v_u == None:
			self.v_u = numpy.zeros(shape=(Np), dtype='float32')
		
		sx = self.scale / float(self.w)
		sy = self.scale / float(self.h)
		
		# Add influence from patch charges
		for i in range(0, Np):
			# Patch potential
			self.v_u[i] = self.e_patches[i][1]
			
			for j in range(0, Np):
				# Patch center coordinates
				cx = (self.e_patches[i][0][0] - 0.5 * self.pw)
				cy = (self.e_patches[i][0][1] - 0.5 * self.ph)
				# Patch bounds
				x1 = self.e_patches[j][0][0]
				x0 = x1 - self.pw
				y1 = self.e_patches[j][0][1]
				y0 = y1 - self.ph
				# Non-singular part
				if i != j:
					self.m_k[i, j] = self.p_patch_ns(cx * sx, cy * sy, x0 * sx, y0 * sy, x1 * sx, y1 * sy)
				# Singular part
				else:
					self.m_k[i, j] = self.p_patch_s(x0 * sx, y0 * sy, x1 * sx, y1 * sy)
		
		numpy.savetxt('m_k.csv', self.m_k, delimiter=',')

	def calc_u(self, x, y):
		Np = len(self.e_patches)
		
		v_k = numpy.matrix(numpy.zeros(shape=(1, Np), dtype='float32'))
		
		# Add influence from patch charges
		for i in range(0, len(self.e_patches)):
			# Patch bounds
			x1 = self.e_patches[i][0][0] * self.pw
			x0 = x1 - self.pw
			y1 = self.e_patches[i][0][1] * self.ph
			y0 = y1 - self.ph
			# Always non-singular
			v_k[0, i] = self.p_patch_ns(x, y, x0, y0, x1, y1)
		
		return v_k.dot(self.v_q)[0, 0]

	def m_calculate(self):
		if self.w == 0 or self.h == 0:
			return
		
		self.calc_coeff_m()

		max_diff = 0
		
		# Cartesian coordinates or Cylindrical coordinates on 2D plane
		if self.coordsys == self.CS_CARTESIAN_2D or self.coordsys == self.CS_CYLINDRICAL:
			numpy.savetxt('v_u.csv', self.v_u, delimiter=',')
			# Solve matrix equation Q = K.I * u
			# Thanks to Jaak Vaabel, who found http://www.johndcook.com/blog/2010/01/19/dont-invert-that-matrix/
			self.v_q = scipy.linalg.solve(self.m_k, self.v_u)
			numpy.savetxt('v_q.csv', self.v_q, delimiter=',')
			
			# Calculating charge?
			if self.Algorithms[self.algorithm] == "Charge":
				# Calculate capacitance
				self.capacitance = 0
				for i in range(0, len(self.v_q)):
					self.capacitance += self.v_q[i] / self.e_patches[i][1]
				self.capacitance *= self.scale**2 * (self.pw * self.ph) / float(self.w * self.h)
				print "Capacitance: {:e} F".format(self.capacitance)
				
				self.min_cvalue = min(self.v_q)
				self.max_cvalue = max(self.v_q)
				
				# Draw patch charge values directly
				for p_id in range(0, len(self.e_patches)):
					p = self.e_patches[p_id]
					x = p[0][0]
					y = p[0][1]
					for j in range(x - self.pw / 2, x + self.pw / 2):
						for i in range(y - self.ph / 2, y + self.ph / 2):
							self.potential[j + i * self.w] = self.v_q[p_id]
			else:
				# Calculate potential for all pixels
				for i in range(0, self.h, 1):
					for j in range(0, self.w, 1):
						self.potential[j + i * self.w] = self.calc_u(j, i)
			
			#self.save_m_csv("potential.csv", self.potential, self.w)
			
		# Use the maximum difference as an estimate for error
		self.error_so_far = max_diff
		
	def m_efield(self):
		self.log.info("Calculating electric field")
		
		if self.libsim_math != None and self.use_cmath:
			self.libsim_math.felectric_field(self.potential, self.efield, self.w, self.h, ctypes.c_float(self.edensity), ctypes.c_bool(self.coordsys == self.CS_CYLINDRICAL))
		else:
			w = self.w
			h = self.h
			for y in range(0, self.h):
				for x in range(0, self.w):
					if x > 0 and x < w -1 and y > 0 and y < h - 1:
						dx = (self.potential[x + 1 + y * w] - self.potential[x - 1 + y * w]) * self.edensity / 2.0
						dy = (self.potential[x + (y + 1) * w] - self.potential[x + (y - 1) * w]) * self.edensity / 2.0
					else:
						# Single-sided derivative
						if x == 0:
							if self.coordsys == self.CS_CYLINDRICAL:
								dx = abs(self.potential[x + (y + 1) * w] - self.potential[x + (y - 1) * w]) * self.edensity / 2.0
							else:
								dx = (self.potential[x + 1 + y * w] - self.potential[x + y * w]) * self.edensity
						elif x == w - 1:
							dx = (self.potential[x + y * w] - self.potential[x - 1 + y * w]) * self.edensity
						if y == 0:
							dy = (self.potential[x + (y + 1) * w] - self.potential[x + y * w]) * self.edensity
						elif y == h - 1:
							dy = (self.potential[x + y * w] - self.potential[x + (y - 1) * w]) * self.edensity
					
					self.efield[x + y * w] = math.sqrt(dx * dx + dy * dy)
		
		# Find minimum and maximum values
		self.min_efvalue = min(self.efield)
		self.max_efvalue = max(self.efield)
		
		self.efield_valid = True
	
	@QtCore.Slot()
	def output_efield(self):
		if not self.efield_valid:
			self.m_efield()
		
		self.log.info("Converting electric field to an image")
		
		# Convert it to an image and emit a "done" signal to display it
		self.to_image("efield.png", self.efield, self.min_efvalue, self.max_efvalue)
		ESim.end_algorithm(self)
	
	@QtCore.Slot()
	def output_potential(self):
		if self.Algorithms[self.algorithm] == "Charge":
			self.log.info("Converting charge field to an image")
			# Convert it to an image and emit a "done" signal to display it
			self.to_image("charge.png", self.potential, self.min_cvalue, self.max_cvalue)
		else:
			self.log.info("Converting potential field to an image")
			# Convert it to an image and emit a "done" signal to display it
			self.to_image("potential.png", self.potential, self.min_value, self.max_value)
		ESim.end_algorithm(self)

	def save_sim(self, f):
		ESim.save_sim(self, f)
		f.write('sim.edensity = {}\nsim.epsilon0 = {}\n'.format(self.edensity, self.epsilon0))

	def get_value(self, x, y):
		if self.Algorithms[self.algorithm] == "Charge":
			value = ESim.get_value(self, x, y) * (self.max_cvalue - self.min_cvalue) / 255.0 + self.min_cvalue
			return "{} C".format(value)
		else:
			value = ESim.get_value(self, x, y) * (self.max_value - self.min_value) / 255.0 + self.min_value
			return "{} V".format(value)
	