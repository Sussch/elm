#!/usr/bin/python
# Indrek Synter 2013

from PySide import QtGui, QtCore, QtXml
import ui.main as main
import main_global as mg
import logging
import optparse
import os, sys

if __name__ == '__main__':
	optp = optparse.OptionParser()
	optp.add_option('-v', '--verbose', dest='verbose', action='count',
					help="Increase verbosity (specify multiple times for more)")
	optp.add_option('-r', '--raster', dest='raster', action='store_true',
					help="Use software rasterizer")
	opts, args = optp.parse_args()

	log_level = logging.INFO # default
	if opts.verbose >= 1:
		log_level = logging.DEBUG

	if opts.raster:
		mg.use_opengl = False
	
	logging.basicConfig(level=log_level, filename='es.log', 
		format='%(asctime)s: %(levelname)s: %(message)s')
	log = logging.getLogger(__name__)
	# NOTE:: basicConfig does not set up StreamHandler when supplied with a filename
	# Add logging to stdout, stderr
	streamhandler = logging.StreamHandler()
	streamhandler.setLevel(log_level)
	log.addHandler(streamhandler)
	
	log.info("\nES launched")
	
	app = QtGui.QApplication(sys.argv)
	# The following might be needed to save ICPTerminal main window
	# state and position
	app.setOrganizationDomain("ut.ee")
	app.setOrganizationName("University of Tartu")
	app.setApplicationName("ES")
	app.setApplicationVersion("1.0")
	
	try:
		# Needed for a frozen PyInstaller package
		if getattr(sys, 'frozen', False):
			mg.basedir = os.path.dirname(sys.executable)
			log.info("Running from a frozen PyInstaller package")
		elif __file__:
			mg.basedir = os.path.dirname(__file__)

		mg.configdir = os.path.join(mg.basedir, 'config')

		main.show()
	except Exception as e:
		mainwin = QtGui.QMainWindow()
		errorDialog = QtGui.QErrorMessage(mainwin)
		errorDialog.setWindowTitle("Error")
		errorDialog.showMessage(str(e))
		log.exception(e)

	app.connect(app, QtCore.SIGNAL("lastWindowClosed()"),
				app, QtCore.SLOT("quit()"))
	app.exec_()
