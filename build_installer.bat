
set PyI=C:\PyInstaller\pyinstaller-2.0
set build=C:\ELM
set outdir=C:\ELM\dist\elm
set tmpdir=C:\ELM\build\pyi.win32\elm
set olddir=%CD%

mkdir %build%\elm
mkdir %build%\elm\ui
mkdir %build%\elm\sim_math
mkdir %build%\elm\config
xcopy /Y ..\pyicp\build\lib\Release\pyicp.pyd icpterm\
xcopy /S /Y ui %build%\elm\ui\
xcopy /Y sim_math\sim_math.dll %build%\elm\sim_math\
xcopy /Y *.py %build%\elm\
xcopy /Y elm.spec %build%\

cd %build%
python %PyI%\pyinstaller.py elm.spec
mkdir dist\elm\config
mkdir dist\elm\sim_math
mkdir dist\elm\ui
mkdir dist\elm\ui\icons
xcopy /S /Y "elm\sim_math" dist\elm\sim_math
xcopy /S /Y "elm\ui\icons" dist\elm\ui\icons
cd %olddir%
