# Indrek Synter 2013

from PySide import QtGui, QtCore
import math

def snap_point_to_grid(grid_step, point):
	new_point = QtCore.QPoint()
	if point.x() % grid_step >= grid_step / 2:
		new_point.setX(point.x() - point.x() % grid_step + grid_step)
	else:
		new_point.setX(point.x() - point.x() % grid_step)
	if point.y() % grid_step >= grid_step / 2:
		new_point.setY(point.y() - point.y() % grid_step + grid_step)
	else:
		new_point.setY(point.y() - point.y() % grid_step)
	return new_point

class ESRect():
	def __init__(self, x0=0, y0=0, x1=0, y1=0, w=None, h=None):
		self.x0 = x0
		self.y0 = y0
		if w == None:
			self.x1 = x1
		else:
			self.x1 = x0 + w
		if h == None:
			self.y1 = y1
		else:
			self.y1 = y0 + h
	
	def width(self):
		return abs(self.x1 - self.x0)
	
	def height(self):
		return abs(self.y1 - self.y0)
	
	def setWidth(self, w):
		self.x1 = self.x0 + w
	
	def setHeight(self, h):
		self.y1 = self.y0 + h
	
	def x(self):
		return self.left()
	
	def y(self):
		return self.top()
	
	def cx(self):
		return self.x0 + self.width() / 2
	
	def cy(self):
		return self.y0 + self.height() / 2
	
	def left(self):
		return min(self.x0, self.x1)
	
	def top(self):
		return min(self.y0, self.y1)
	
	def right(self):
		return max(self.x0, self.x1)
	
	def bottom(self):
		return max(self.y0, self.y1)
	
	def setRect(self, x0, y0, x1, y1):
		self.x0 = x0
		self.y0 = y0
		self.x1 = x1
		self.y1 = y1

	def toQRect(self):
		mx = min(self.x0, self.x1)
		my = min(self.y0, self.y1)
		return QtCore.QRect(mx, my, self.width(), self.height())
	
	def snap(self, grid_step):
		p0 = snap_point_to_grid(grid_step, QtCore.QPoint(self.x0, self.y0))
		p1 = snap_point_to_grid(grid_step, QtCore.QPoint(self.x1, self.y1))
		self.x0 = p0.x()
		self.y0 = p0.y()
		self.x1 = p1.x()
		self.y1 = p1.y()
	
	def scale(self, sx, sy):
		self.x0 /= sx
		self.x1 /= sx
		self.y0 /= sy
		self.y1 /= sy

class ESGraphicsScene(QtGui.QGraphicsScene):
	sig_process_drag_rect = QtCore.Signal()
	sig_create_rect = QtCore.Signal(int, int, int, int, float)
	sig_create_ellipse = QtCore.Signal(int, int, int, int, float)
	sig_create_point_charge = QtCore.Signal(int, int, float)
	sig_create_control_point = QtCore.Signal(int, int, float)
	sig_create_line_charge = QtCore.Signal(int, int, int, int, float)
	sig_create_circle_charge = QtCore.Signal(int, int, int, float)
	sig_create_poly = QtCore.Signal(list, float)
	sig_update_cursor = QtCore.Signal(float, float, str)
	
	POINT_SIZE = 6
	ELEMENT_COLOR = QtGui.QColor(68, 156, 201, 128)
	LINE_COLOR = QtGui.QColor(58, 133, 172)
	DRAGRECT_COLOR = QtGui.QColor(100, 100, 200)
	
	def __init__(self, simulation, scale):
		QtGui.QGraphicsScene.__init__(self)
		
		self.setSceneRect(0, 0, 600, 600)
		
		self.cur_tool = "Select"
		
		self.grid_a = 10.0
		self.snap_to_grid = True
		self.grid_points = []
		self.scale = scale
		
		self.dragging = False
		self.drag_rect = ESRect()
		self.dragging_poly = False
		self.poly_points = []
		
		self.potential = 100.0;
		
		self.commands = ""

		self.simulation = None
		self.set_simulation(simulation, scale)
	
	def init(self):
		self.grid_points = []
		self.clear()
		
		rect = self.sceneRect()
		
		# Add a background rect
		self.bg = QtGui.QGraphicsRectItem(rect)
		self.bg.setBrush(QtGui.QBrush(QtGui.QColor(0, 0, 0)))
		self.bg.setPen(QtCore.Qt.NoPen)
		self.addItem(self.bg)
		
		# Add a drag rect indicator to the scene
		self.drag_rect_indicator = QtGui.QGraphicsRectItem()
		self.drag_rect_indicator.setBrush(QtCore.Qt.NoBrush)
		self.drag_rect_indicator.setPen(QtGui.QPen(self.DRAGRECT_COLOR))
		self.addItem(self.drag_rect_indicator)
		
		# Add polygon indicator to the scene
		brush = QtGui.QBrush(self.ELEMENT_COLOR)
		pen = QtGui.QPen(self.LINE_COLOR)
		self.poly_item = QtGui.QGraphicsPolygonItem()
		self.poly_item.setBrush(brush)
		self.poly_item.setPen(pen)
		self.addItem(self.poly_item)
		self.poly_item.hide()

		self.sig_process_drag_rect.connect(self.process_drag_rect)
		
	def show_grid(self, enabled):
		if enabled:
			for p in self.grid_points:
				p.show()
			# No grid points? Generate them
			if self.grid_points == []:
				self.set_grid(grid_a=self.grid_a)
		else:
			for p in self.grid_points:
				p.hide()
	
	def snap_grid(self, enabled):
		self.snap_to_grid = enabled
	
	def set_grid(self, grid_a_scaled=None, grid_a=None):
		rect = self.sceneRect()
		
		self.show_grid(False)
		if grid_a != None and grid_a_scaled == None:
			self.grid_a = grid_a
		else:
			self.grid_a = grid_a_scaled * rect.width() / self.scale
		self.grid_points = []
		
		# Too dense a grid?
		if self.grid_a < 3:
			return
		
		# Add grid points
		brush = QtGui.QBrush(QtGui.QColor(200, 200, 200))
		
		y = 0
		while y < rect.height():
			y += self.grid_a
			x = 0
			while x < rect.width():
				x += self.grid_a
				
				self.grid_points.append(self.addEllipse(x - 1, y - 1, 1, 1, QtCore.Qt.NoPen, brush))

	# Get the scene vs. simulation scaling factors
	def get_scale(self):
		s = QtCore.QPointF()
		s.setX(self.sceneRect().width() / float(self.simulation.width()))
		s.setY(self.sceneRect().height() / float(self.simulation.height()))
		return s
	
	@QtCore.Slot(QtGui.QImage)
	def update_image(self, image):
		brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
		if image != None:
			transform = QtGui.QTransform()
			s = self.get_scale()
			transform.scale(s.x(), s.y())
			brush.setTextureImage(image)
			brush.setTransform(transform)
		
		self.bg.setBrush(brush)
		self.update()
	
	def mousePressEvent(self, event):
		target = self.itemAt(event.scenePos())
		modifiers = QtGui.QApplication.keyboardModifiers()
		
		p = event.scenePos()
		if self.snap_to_grid:
			p = snap_point_to_grid(self.grid_a, p)
		
		if event.button() == QtCore.Qt.MouseButton.LeftButton:
			if self.cur_tool in ['PointCharge', 'ControlPoint']:
				self.create_point(p)
			elif self.cur_tool == 'Poly':
				self.poly_add_point(p)
			else:
				self.dragging = True
				self.drag_rect.setRect(p.x(), p.y(), p.x(), p.y())
			#if modifiers == QtCore.Qt.ShiftModifier:
			#elif modifiers == QtCore.Qt.ControlModifier:
		# Right mouse click clears the highlights
		elif event.button() == QtCore.Qt.MouseButton.RightButton:
			if self.cur_tool == 'Poly':
				self.poly_close()
	
	def update_drag_rect(self, x, y):
		self.drag_rect.setWidth(x - self.drag_rect.x0)
		self.drag_rect.setHeight(y - self.drag_rect.y0)
	
	def mouseReleaseEvent(self, event):
		p = event.scenePos()
		if self.snap_to_grid:
			p = snap_point_to_grid(self.grid_a, p)
		
		if event.button() == QtCore.Qt.MouseButton.LeftButton:
			self.drag_rect_indicator.setPen(QtCore.Qt.NoPen)
			self.update_drag_rect(p.x(), p.y())
			self.process_drag_rect()
		elif event.button() == QtCore.Qt.MouseButton.RightButton:
			pass
	
	def mouseMoveEvent(self, event):
		p = event.scenePos()
		if self.snap_to_grid:
			p = snap_point_to_grid(self.grid_a, p)
		
		if self.dragging:
			self.update_drag_rect(p.x(), p.y())
			self.drag_rect_indicator.setRect(self.drag_rect.toQRect())
			self.drag_rect_indicator.setPen(QtGui.QPen(self.DRAGRECT_COLOR))
		elif self.dragging_poly and self.cur_tool == 'Poly':
			self.poly_drag(p)
		
		# Update cursor info
		value = 0.0
		if self.simulation != None:
			value = self.simulation.get_value(event.scenePos().x(), event.scenePos().y())
		sp = QtCore.QPointF(event.scenePos().x(), event.scenePos().y())
		sp.setX(self.scale * sp.x() / float(self.sceneRect().width()))
		sp.setY(self.scale * sp.y() / float(self.sceneRect().height()))
		self.sig_update_cursor.emit(sp.x(), sp.y(), value)

	def wheelEvent(self, event):
		print "Wheel"
	
	@QtCore.Slot(str)
	def tool_selected(self, tool):
		self.cur_tool = tool
	
	@QtCore.Slot(float)
	def potential_changed(self, potential):
		self.potential = potential

	@QtCore.Slot()
	def process_drag_rect(self):
		# Snap to grid?
		if self.snap_to_grid:
			self.drag_rect.snap(self.grid_a)
		# Stopped dragging
		self.dragging = False
		
		if self.cur_tool == 'Rectangle':
			# Invalid rect?
			if self.drag_rect.width() < 1 or self.drag_rect.height() < 1:
				return
			self.create_rect(self.drag_rect)
		elif self.cur_tool == 'Ellipse':
			# Invalid rect?
			if self.drag_rect.width() < 1 or self.drag_rect.height() < 1:
				return
			self.create_ellipse(self.drag_rect)
		elif self.cur_tool == 'CircleCharge':
			# Invalid rect?
			if self.drag_rect.width() < 1 or self.drag_rect.height() < 1:
				return
			self.create_circle(self.drag_rect)
		elif self.cur_tool == 'LineCharge':
			self.create_line(self.drag_rect)
			
	def electrode_rect(self, x0, y0, x1, y1, potential):
		self.cur_tool = 'Rectangle'
		self.potential = potential
		self.create_rect(ESRect(x0, y0, w=x1, h=y1))

	def electrode_ellipse(self, x0, y0, x1, y1, potential):
		self.cur_tool = 'Ellipse'
		self.potential = potential
		self.create_ellipse(ESRect(x0, y0, w=x1, h=y1))
	
	def electrode_poly(self, points, potential):
		self.cur_tool = 'Poly'
		self.potential = potential
		for p in points:
			self.poly_add_point(QtCore.QPoint(p[0], p[1]))
		self.poly_close()

	def charge_point(self, x, y, potential):
		self.cur_tool = 'PointCharge'
		self.potential = potential
		self.create_point(QtCore.QPoint(x, y))

	def control_point(self, x, y, potential):
		self.cur_tool = 'ControlPoint'
		self.potential = potential
		self.create_point(QtCore.QPoint(x, y))

	def charge_line(self, x0, y0, x1, y1, potential):
		self.cur_tool = 'LineCharge'
		self.potential = potential
		self.create_line(ESRect(x0, y0, w=x1, h=y1))
		
	def charge_circle(self, x0, y0, x1, y1, potential):
		self.cur_tool = 'CircleCharge'
		self.potential = potential
		self.create_circle(ESRect(x0, y0, w=x1, h=y1))
	
	def create_rect(self, rect):
		# Add the rect to the scene
		brush = QtGui.QBrush(self.ELEMENT_COLOR)
		pen = QtGui.QPen(self.LINE_COLOR)
		qitem = QtGui.QGraphicsRectItem(rect.toQRect())
		qitem.setBrush(brush)
		qitem.setPen(pen)
		self.addItem(qitem)
		
		self.commands += "scene.electrode_rect({}, {}, {}, {}, {})\n".format(rect.left(), rect.top(), rect.width(), rect.height(), self.potential)
		
		# Scene and simulation might have different resolutions,
		# so we'll scale it here
		s = self.get_scale()
		rect.scale(s.x(), s.y())
		self.sig_create_rect.emit(rect.left(), rect.top(), rect.right(), rect.bottom(), self.potential)

	def create_ellipse(self, rect):
		# Add the ellipse to the scene
		brush = QtGui.QBrush(self.ELEMENT_COLOR)
		pen = QtGui.QPen(self.LINE_COLOR)
		qitem = QtGui.QGraphicsEllipseItem(rect.toQRect())
		qitem.setBrush(brush)
		qitem.setPen(pen)
		self.addItem(qitem)
		
		self.commands += "scene.electrode_ellipse({}, {}, {}, {}, {})\n".format(rect.left(), rect.top(), rect.width(), rect.height(), self.potential)
		
		# Scene and simulation might have different resolutions,
		# so we'll scale it here
		s = self.get_scale()
		rect.scale(s.x(), s.y())
		self.sig_create_ellipse.emit(rect.left(), rect.top(), rect.right(), rect.bottom(), self.potential)
		
	def create_point(self, point):
		# Choose color, based on the selected tool
		if self.cur_tool == 'PointCharge':
			pen_color = self.LINE_COLOR
			fill_color = self.ELEMENT_COLOR
		elif self.cur_tool == 'ControlPoint':
			pen_color = QtGui.QColor(73, 138, 0)
			fill_color = QtGui.QColor(126, 190, 0, 128)
		else:
			fill_color = QtGui.QColor(220, 220, 220, 128)
			pen_color = QtGui.QColor(0, 0, 0)
		
		# Add a small circle to the scene
		brush = QtGui.QBrush(fill_color)
		pen = QtGui.QPen(pen_color)
		qitem = QtGui.QGraphicsEllipseItem(QtCore.QRect(point.x() - self.POINT_SIZE / 2, point.y() - self.POINT_SIZE / 2, self.POINT_SIZE, self.POINT_SIZE))
		qitem.setBrush(brush)
		qitem.setPen(pen)
		self.addItem(qitem)
		
		if self.cur_tool == 'PointCharge':
			self.commands += "scene.charge_point({}, {}, {})\n".format(point.x(), point.y(), self.potential)
			
			# Scene and simulation might have different resolutions,
			# so we'll scale it here
			s = self.get_scale()
			self.sig_create_point_charge.emit(point.x() / s.x(), point.y() / s.y(), self.potential)

		elif self.cur_tool == 'ControlPoint':
			self.commands += "scene.control_point({}, {}, {})\n".format(point.x(), point.y(), self.potential)

			# Scene and simulation might have different resolutions,
			# so we'll scale it here
			s = self.get_scale()
			self.sig_create_control_point.emit(point.x() / s.x(), point.y() / s.y(), self.potential)

	def create_line(self, rect):
		# Add a line to the scene
		pen = QtGui.QPen(self.LINE_COLOR)
		qitem = QtGui.QGraphicsLineItem(rect.x0, rect.y0, rect.x1, rect.y1)
		qitem.setPen(pen)
		self.addItem(qitem)
		
		if self.cur_tool == 'LineCharge':
			self.commands += "scene.charge_line({}, {}, {}, {}, {})\n".format(rect.x0, rect.y0, rect.x1, rect.y1, self.potential)
			
			# Scene and simulation might have different resolutions,
			# so we'll scale it here
			s = self.get_scale()
			rect.scale(s.x(), s.y())
			self.sig_create_line_charge.emit(rect.x0, rect.y0, rect.x1, rect.y1, self.potential)

	def create_circle(self, rect):
		# Add the circle to the scene
		pen = QtGui.QPen(self.LINE_COLOR)
		minw = min(rect.width(), rect.height())
		qitem = QtGui.QGraphicsEllipseItem(rect.x(), rect.y(), minw, minw)
		qitem.setBrush(QtCore.Qt.NoBrush)
		qitem.setPen(pen)
		self.addItem(qitem)
		
		if self.cur_tool == 'CircleCharge':
			self.commands += "scene.charge_circle({}, {}, {}, {})\n".format(rect.left(), rect.top(), rect.right(), rect.bottom(), self.potential)
			
			# Scene and simulation might have different resolutions,
			# so we'll scale it here
			s = self.get_scale()
			rect.scale(s.x(), s.y())
			self.sig_create_circle_charge.emit(rect.cx(), rect.cy(), rect.width() / 2, rect.height() / 2, self.potential)
			
	def poly_add_point(self, point):
		if not self.dragging_poly:
			self.poly_points = []
		self.dragging_poly = True
		self.poly_points.append(point)
		self.poly_item.setPolygon(QtGui.QPolygonF(self.poly_points))
		self.poly_item.show()
	
	def poly_drag(self, point):
		self.poly_item.setPolygon(QtGui.QPolygonF(self.poly_points + [point]))
		self.poly_item.show()
	
	def poly_close(self):
		self.dragging_poly = False
		self.poly_item.hide()
		
		# Add the poly to the scene
		brush = QtGui.QBrush(self.ELEMENT_COLOR)
		pen = QtGui.QPen(self.LINE_COLOR)
		qitem = QtGui.QGraphicsPolygonItem(QtGui.QPolygonF(self.poly_points))
		qitem.setBrush(brush)
		qitem.setPen(pen)
		self.addItem(qitem)
		
		pp = []
		for p in self.poly_points:
			pp.append([p.x(), p.y()])
		self.commands += "scene.electrode_poly({}, {})\n".format(pp, self.potential)
		
		# Scene and simulation might have different resolutions,
		# so we'll scale it here
		s = self.get_scale()
		for i in range(0, len(self.poly_points)):
			self.poly_points[i].setX(self.poly_points[i].x() / s.x())
			self.poly_points[i].setY(self.poly_points[i].y() / s.y())
		self.sig_create_poly.emit(self.poly_points, self.potential)

	def set_simulation(self, simulation, scale):
		if self.simulation != None:
			self.sig_create_rect.disconnect(self.simulation.draw_rect)
			self.sig_create_ellipse.disconnect(self.simulation.draw_ellipse)
			self.sig_create_point_charge.disconnect(self.simulation.create_point_charge)
			self.sig_create_line_charge.disconnect(self.simulation.create_line_charge)
			self.sig_create_circle_charge.disconnect(self.simulation.create_circle_charge)
			self.sig_create_control_point.disconnect(self.simulation.create_control_point)
		
		self.commands = ""
		
		self.simulation = simulation
		self.simulation.create(600, 600, scale)
		self.init()
		self.simulation.algorithm_done.connect(self.update_image)
		
		self.sig_create_rect.connect(self.simulation.draw_rect)
		self.sig_create_ellipse.connect(self.simulation.draw_ellipse)
		self.sig_create_point_charge.connect(self.simulation.create_point_charge)
		self.sig_create_line_charge.connect(self.simulation.create_line_charge)
		self.sig_create_circle_charge.connect(self.simulation.create_circle_charge)
		self.sig_create_control_point.connect(self.simulation.create_control_point)

	def get_commands(self):
		return self.commands