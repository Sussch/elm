# Indrek Synter 2013
from PySide import QtGui, QtCore, QtOpenGL
import logging

import main_global as mg

class ESGraphicsView(QtGui.QGraphicsView):
	def __init__(self):
		QtGui.QGraphicsView.__init__(self)
		
		self.log = logging.getLogger(__name__)
		
		if mg.use_opengl:
			self.log.info("Setting up OpenGL viewport")
			self.setViewport(QtOpenGL.QGLWidget())
		else:
			self.log.info("Using default rasterizer")
			
		# Need this to track mouse movement when no buttons are pressed
		self.setMouseTracking(True)
