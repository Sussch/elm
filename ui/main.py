# Indrek Synter 2013

from PySide import QtGui, QtCore, QtUiTools
import os, sys
import logging
import traceback

import numpy, scipy

# Needed to inject a custom QMainWindow into the UI
import pyside_dynamic

import main_global as mg

from graphics_scene import ESGraphicsScene
from graphics_view import ESGraphicsView
from sim_fdm import ESimFD
from sim_fem import ESimFE
from sim_csm import ESimC
from sim_ssm import ESimSC

def show():
	mainwin = ESMainWindow()
	mainwin.show()

class ESMainWindow(QtGui.QMainWindow):
	sig_tool_selected = QtCore.Signal(str)
	sig_potential_changed = QtCore.Signal(float)
	sig_show_potential = QtCore.Signal(bool)
	sig_show_efield = QtCore.Signal(bool)
	sig_update_image = QtCore.Signal()
	
	TOOLS = ['Select', 'Rectangle', 'Ellipse', 'Poly', 'PointCharge', 'LineCharge', 'CircleCharge', 'ControlPoint']
	METHODS = ['Fixed Distance', 'Fixed Elements', 'Charge Simu', 'Surf Charge']
	UNITS = [1, 1E-3, 1E-6]
	
	def __init__(self, parent=None):
		QtGui.QMainWindow.__init__(self, parent)

		self.simulation = None
		self.scene = None
		self.method = 0
		self.sim_scale = self.UNITS[0]

		# Load the UI
		file = QtCore.QFile(os.path.join(mg.basedir, "ui/main.ui"))
		file.open(QtCore.QFile.ReadOnly)
		loader = pyside_dynamic.UiLoader(self)
		self.content = loader.load(file)
		QtCore.QMetaObject.connectSlotsByName(self.content)
		file.close()
		
		self.log = logging.getLogger(__name__)
		
		# Restore the last state of the UI
		self.state = QtCore.QSettings(os.path.join(mg.configdir, 'windowState.ini'), QtCore.QSettings.IniFormat, self)
		self.restore_state()
		
		self.init_graphics()
		self.init_actions()
		self.init_namespace()
		
	def init_actions(self):
		self.content.toolGroup1 = QtGui.QButtonGroup()
		self.content.toolGroup1.addButton(self.content.toolSelect, 0)
		self.content.toolGroup1.addButton(self.content.toolRectangle, 1)
		self.content.toolGroup1.addButton(self.content.toolEllipse, 2)
		self.content.toolGroup1.addButton(self.content.toolPoly, 3)
		self.content.toolGroup1.addButton(self.content.toolPointCharge, 4)
		self.content.toolGroup1.addButton(self.content.toolLineCharge, 5)
		self.content.toolGroup1.addButton(self.content.toolCircleCharge, 6)
		self.content.toolGroup1.addButton(self.content.toolControlPoint, 7)
		self.content.toolGroup1.buttonClicked.connect(self.tool_selected)
		self.content.spinPotential.valueChanged.connect(self.potential_changed)
		
		self.content.runButton.clicked.connect(self.do_run)
		self.content.stopButton.clicked.connect(self.do_stop)
		self.content.spinItTotal.valueChanged.connect(self.num_iterations_changed)
		self.content.spinItError.valueChanged.connect(self.error_tolerance_changed)
		
		self.content.checkCMath.stateChanged.connect(self.cmath_enable_changed)
		self.content.comboCoordsys.currentIndexChanged.connect(self.coordsys_changed)
		self.content.comboMethod.currentIndexChanged.connect(self.method_changed)
		
		self.content.pushCalcEField.clicked.connect(self.do_calc_efield)
		
		self.content.comboUnits.currentIndexChanged.connect(self.update_sim_scale)
		self.content.spinSideLen.valueChanged.connect(self.update_sim_scale)
		
		self.content.checkShowGrid.toggled.connect(self.grid_show)
		self.content.checkSnapToGrid.toggled.connect(self.grid_snap)
		self.content.spinGridSpacing.valueChanged.connect(self.grid_set)
		
		self.efield_action = QtGui.QAction(QtGui.QIcon('ui/icons/cargo.png'), 'EField', self)
		self.efield_action.setCheckable(True)
		self.efield_action.triggered.connect(self.switch_efield)
		
		self.toolbar = self.addToolBar('Toolbar')
		self.toolbar.addAction(self.efield_action)
		
		self.content.action_Open.triggered.connect(self.browse_sim_in)
		self.content.action_Save.triggered.connect(self.browse_sim_out)
		self.content.action_New.triggered.connect(self.new_sim)
		self.content.actionE_xit.triggered.connect(self.close)
		
		self.cursor_info = QtGui.QLabel('Cursor info')
		self.statusBar().addPermanentWidget(self.cursor_info)
		
		self.grid_set(self.content.spinGridSpacing.value())
	
	def init_graphics(self):
		# A work-around to a PySide crashbug on custom widgets from UI file
		self.content.graphicsView = ESGraphicsView()
		layout = QtGui.QVBoxLayout()
		layout.setContentsMargins(0, 0, 0, 0)
		layout.addWidget(self.content.graphicsView)
		self.content.viewContainer.setLayout(layout)
	
		self.init_sim(self.content.comboMethod.currentIndex())
	
		self.scene = ESGraphicsScene(self.simulation, self.sim_scale)
		self.content.graphicsView.setScene(self.scene)
	
		self.sig_tool_selected.connect(self.scene.tool_selected)
		self.sig_potential_changed.connect(self.scene.potential_changed)
		self.sig_update_image.connect(self.scene.update_image)
		self.scene.sig_update_cursor.connect(self.update_cursor)
	
	def new_sim(self):
		self.log.info("Starting a new simulation")
		
		self.init_sim(self.content.comboMethod.currentIndex())

	def init_sim(self, method):
		self.do_stop()

		self.method = method
		self.content.comboAlgorithm.clear()
		if self.METHODS[method] == 'Fixed Distance':
			self.simulation = ESimFD()
			self.content.comboAlgorithm.addItems(ESimFD.Algorithms)
		elif self.METHODS[method] == 'Fixed Elements':
			self.simulation = ESimFE()
			self.content.comboAlgorithm.addItems(ESimFE.Algorithms)
		elif self.METHODS[method] == 'Charge Simu':
			self.simulation = ESimC()
			self.content.comboAlgorithm.addItems(ESimC.Algorithms)
		elif self.METHODS[method] == 'Surf Charge':
			self.simulation = ESimSC()
			self.content.comboAlgorithm.addItems(ESimSC.Algorithms)
		else:
			return

		self.num_iterations_changed(self.content.spinItTotal.value())
		self.error_tolerance_changed(self.content.spinItError.value())
		self.simulation.iteration_done.connect(self.simit_done)
		self.simulation.algorithm_done.connect(self.sim_done)
		self.sig_show_efield.connect(self.simulation.output_efield)
		self.sig_show_potential.connect(self.simulation.output_potential)
		if self.scene != None:
			self.scene.set_simulation(self.simulation, self.sim_scale)
	
	def tool_selected(self, tool):
		tool_id = self.content.toolGroup1.checkedId()
		self.sig_tool_selected.emit(self.TOOLS[tool_id])
		
	def potential_changed(self, potential):
		self.sig_potential_changed.emit(potential)
		
	def grid_show(self, enabled):
		self.scene.show_grid(enabled)

	def grid_snap(self, enabled):
		self.scene.snap_grid(enabled)
	
	def grid_set(self, value):
		self.scene.set_grid(value)
	
	def set_grid(self, density, do_show, do_snap):
		self.grid_set(density)
		self.grid_show(do_show)
		self.grid_snap(do_snap)
		
	def set_sim_scale(self, sidelen, units):
		self.content.comboUnits.setCurrentIndex(units)
		self.content.spinSideLen.setValue(sidelen)
	
	def do_run(self):
		self.content.labelEFieldValid.setText("Out of date")
		self.simulation.set_algorithm(self.content.comboAlgorithm.currentIndex())
		self.simulation.start()
		
	def do_stop(self):
		if self.simulation != None:
			self.simulation.stop()
		
	def simit_done(self):
		self.content.labelItDone.setText(str(self.simulation.get_iteration_count()))
		self.content.labelItError.setText("{:.5g}".format(self.simulation.get_error_margin()))

	def sim_done(self, image):
		pass
	
	def update_cursor(self, x, y, value):
		self.cursor_info.setText("{:.04f} {:.04f}: {}".format(x, y, value))
		
	def num_iterations_changed(self, num_iterations):
		self.simulation.set_num_iterations(num_iterations)

	def error_tolerance_changed(self, tolerance):
		self.simulation.set_error_tolerance(10 ** tolerance)
		
	def cmath_enable_changed(self, cmath_enable):
		self.simulation.use_cmath = cmath_enable
		
	def coordsys_changed(self, index):
		self.simulation.set_coordsys(index)
	
	def update_sim_scale(self, unused):
		self.sim_scale = self.content.spinSideLen.value()
		self.sim_scale *= self.UNITS[self.content.comboUnits.currentIndex()]
		self.simulation.set_scale(self.sim_scale)
	
	def method_changed(self, index):
		self.init_sim(index)
		
	def switch_efield(self):
		if self.efield_action.isChecked():
			self.sig_show_efield.emit(True)
			#self.simulation.output_efield()
			self.content.labelEFieldValid.setText("Up to date")
		else:
			self.sig_show_potential.emit(True)
			#self.simulation.output_potential()
	
	def do_calc_efield(self):
		self.sig_show_efield.emit(True)
		#self.simulation.m_efield()
		self.content.labelEFieldValid.setText("Up to date")
		
	def init_namespace(self):
		self.ns = {'init_sim':self.init_sim, 'sim':self.simulation, 'scene':self.scene}
		self.ns['set_coordsys'] = self.set_coordsys
		self.ns['set_algorithm'] = self.set_algorithm
		self.ns['set_num_iterations'] = self.set_num_iterations
		self.ns['set_error_tolerance'] = self.set_error_tolerance
		self.ns['set_sim_scale'] = self.set_sim_scale
		self.ns['set_grid'] = self.set_grid
	
	def set_coordsys(self, coordsys):
		self.content.comboCoordsys.setCurrentIndex(coordsys)
	
	def set_algorithm(self, algorithm):
		self.content.comboAlgorithm.setCurrentIndex(algorithm)
	
	def set_num_iterations(self, num_iterations):
		self.content.spinItTotal.setValue(num_iterations)
	
	def set_error_tolerance(self, error_tolerance):
		self.content.spinItError.setValue(error_tolerance)
		
	def browse_sim_in(self):
		# Default to local examples directory in case the path editbox
		# is empty
		seekpath = os.path.dirname(os.path.join(mg.basedir, "examples/"))
		sim_path = QtGui.QFileDialog.getOpenFileName(self, "Open a simulation", seekpath, "Python script (*.py)")
		if len(sim_path) > 0 and len(sim_path[0]) > 0:
			self.load_sim(sim_path[0])

	def browse_sim_out(self):
		# Default to local examples directory in case the path editbox
		# is empty
		seekpath = os.path.dirname(os.path.join(mg.basedir, "examples/"))
		sim_path = QtGui.QFileDialog.getSaveFileName(self, "Save simulation", seekpath, "Python script (*.py)")
		if len(sim_path) > 0 and len(sim_path[0]) > 0:
			self.save_sim(sim_path[0])
		
	@QtCore.Slot(str)
	def load_sim(self, filename):
		self.log.info('Loading simulation "{}"'.format(filename))
		
		try:
			execfile(filename, self.ns)
		except Exception as e:
			message = 'Error invoking "{}" on script: {} {}'.format(filename, str(e), str(traceback.format_exc()))
			self.log.error(message)
	
	@QtCore.Slot(str)
	def save_sim(self, filename):
		self.log.info('Saving simulation "{}"'.format(filename))
		
		with open(filename, "wt") as f:
			f.write("init_sim({})\n".format(self.method))
			self.simulation.save_sim(f)
			f.write("\nset_coordsys({})\nset_algorithm({})\nset_num_iterations({})\nset_error_tolerance({})\n".format(self.content.comboCoordsys.currentIndex(), self.content.comboAlgorithm.currentIndex(), self.content.spinItTotal.value(), self.content.spinItError.value()))
			f.write("set_sim_scale({}, {})\n".format(self.content.spinSideLen.value(), self.content.comboUnits.currentIndex()))
			f.write("set_grid({}, {}, {})\n".format(self.content.spinGridSpacing.value(), self.content.checkShowGrid.isChecked(), self.checkSnapToGrid.isChecked()))
			f.write("\n{}".format(self.scene.get_commands()))

	def restore_state(self):
		value = self.state.value("mainWindowGeometry")
		if value is not None:
			self.restoreGeometry(value)
		value = self.state.value("mainWindowState")
		if value is not None:
			self.restoreState(value)
	
	def save_state(self):
		self.state.setValue("mainWindowGeometry", self.saveGeometry())
		self.state.setValue("mainWindowState", self.saveState())
	
	def about(self):
		# TODO Show a small "about" dialog.
		pass

	def closeEvent(self, event):
		self.save_state()
