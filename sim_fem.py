from PySide import QtGui, QtCore

from simulation import ESim

import ctypes
import numpy, scipy
import math

# TODO:: Use scipy.sparse
class NZMatrix():
	def __init__(self, rows, cols):
		self.rows = rows
		self.cols = cols
		self.M = [[]]
		self.L = [[]]
	
	def get_elem(self, row, col):
		if row >= 0 and row < len(self.L) and col >= 0 and col < self.cols:
			# Number of columns on that row
			lenL = len(self.L[row])
			# Find the element coordinates in the legend
			for i in range(0, lenL):
				x = self.L[row][i]
				if x == col:
					return self.M[row][i]
		# All other elements are zeroes
		return 0
	
	def set_elem(self, row, col, value):
		if row >= 0 and row < self.rows and col >= 0 and col < self.cols:
			# Need a new row?
			if len(self.L) <= row:
				for i in range(0, row - len(self.L) + 1):
					self.L.append([])
					self.M.append([])
			# Number of columns on that row
			lenL = len(self.L[row])
			# Find the element coordinates in the legend
			for i in range(0, lenL):
				x = self.L[row][i]
				# Found it?
				if x == col:
					# Should we zero (that is, delete) the element?
					if value == 0:
						self.M[row].remove(self.M[row][i])
						self.L[row].remove(x)
					# Or just replace it?
					else:
						self.M[row][i] = value
					return
			# Element not found :( .. gotta add it, then
			self.L[row].append(col)
			self.M[row].append(value)
			
	def save_csv(self, filename):
		with open(filename, "wt") as f:
			for i in range(0, self.rows):
				line = ""
				for j in range(0, self.cols):
					line += "{:f},".format(self.get_elem(j, i))
				f.write(line + "\n")
	
	def print_m(self):
		for i in range(0, self.rows):
			line = ""
			for j in range(0, self.cols):
				line += "{:f},".format(self.get_elem(j, i))
			print line

# Render a 3-color triangle using a barycentric algorithm
# http://www.sunshine2k.de/coding/java/TriangleRasterization/TriangleRasterization.html
# http://www.scratchapixel.com/lessons/3d-basic-lessons/lesson-9-ray-triangle-intersection/barycentric-coordinates/
def render_triangle_to_image(image, v1, v2, v3, c1, c2, c3):
	# Get triangle bounding box
	maxX = max(v1[0], v2[0], v3[0])
	maxY = max(v1[1], v2[1], v3[1])
	minX = min(v1[0], v2[0], v3[0])
	minY = min(v1[1], v2[1], v3[1])
	# Edge vectors
	vs1 = [v2[0] - v1[0], v2[1] - v1[1]]
	vs2 = [v3[0] - v1[0], v3[1] - v1[1]]
	# For each point
	for y in range(minY, maxY):
		for x in range(minX, maxX):
			# Point vector
			vp = [x - v1[0], y - v1[1]]
			# Cross products
			s = (vp[0] * vs2[1] - vp[1] * vs2[0])
			t = (vs1[0] * vp[1] - vs1[1] * vp[0])
			w = (vs1[0] * vs2[1] - vs1[1] * vs2[0])
			# Point inside the triangle?
			if s >= 0 and t >= 0 and s + t <= w:
				# Interpolate color with the help of barycentric coordinates
				color = (c1 * max(w - s - t, 0) + c2 * s + c3 * t) / abs(w)
				image.setPixel(x, y, color)

"""
Fixed number of elements.
"""
class ESimFE(ESim):
	M_GAUSS = 0
	M_CHOLESKY = 1
	M_GAUSS_SEIDEL = 2
	M_OVER_RELAXATION = 3
	
	MF_FIXED = 0x80000000
	
	Algorithms = ["Gauss", "Cholesky", "Gauss Seidel", "Over-relaxation"]
	
	def __init__(self):
		ESim.__init__(self)

		self.potential = None
		self.system_m = None
		self.ch_triangle = None
		self.efield = None
		self.efield_valid = False
		self.metadata = None
		self.edensity = 10.0
		
		self.ew = 16
		self.eh = 16
		self.es = [1, 1]
		
		self.min_value = 0
		self.max_value = 0
		self.min_efvalue = 0
		self.max_efvalue = 0
		
		self.algorithm = self.M_GAUSS
	
	# Gets the number of elements
	def get_num_elems(self):
		return (2 * (self.ew - 1) * (self.eh - 1))
	
	# Calculates point indexes from element index
	def get_point_ids(self, element_index):
		# Index of the first point of the element
		a = element_index / 2 + element_index / (2 * (self.ew - 1))
		# Even element?
		if element_index % 2 == 0:
			return [a, a + self.ew + 1, a + self.ew]
		# Odd element?
		else:
			return [a, a + 1, a + self.ew + 1]
	
	# Calculates point coordinates from element index
	def get_point_coords(self, element_index):
		# Get point indexes
		ids = self.get_point_ids(element_index)
		
		a = [(ids[0] % self.ew) / self.es[0], (ids[0] / self.ew) / self.es[1]]
		b = [(ids[1] % self.ew) / self.es[0], (ids[1] / self.ew) / self.es[1]]
		c = [(ids[2] % self.ew) / self.es[0], (ids[2] / self.ew) / self.es[1]]
		return [a, b, c]
	
	# Calculates the matrix of an element
	def get_element_m(self, element_index):
		p = self.get_point_coords(element_index)
		# D = bx cy - by cx - ax cy + ay cx + ax by - ay bx
		# 10 21 - 11 20 - 00 21 + 01 20 + 00 11 - 01 10
		D = p[1][0] * p[2][1] - p[1][1] * p[2][0] - p[0][0] * p[2][1] + p[0][1] * p[2][0] + p[0][0] * p[1][1] - p[0][1] * p[1][0]
		
		if self.coordsys == self.CS_CARTESIAN_2D:
			R = 0.5
		elif self.coordsys == self.CS_CYLINDRICAL:
			# R = pi/3 (ar + br + cr)
			R = math.pi * (p[0][0] + p[1][0] + p[2][0]) / 3.0
		
		# 3x3 matrix
		M = (ctypes.c_float * 9)(0)
		
		if D != 0:
			# 00, [(by - cy)^2 + (cx - bx)^2]
			M[0] = R * ((p[1][1] - p[2][1])*(p[1][1] - p[2][1]) + (p[2][0] - p[1][0])*(p[2][0] - p[1][0])) / D
			# 11, [(cy - ay)^2 + (ax - cx)^2]
			M[4] = R * ((p[2][1] - p[0][1])*(p[2][1] - p[0][1]) + (p[0][0] - p[2][0])*(p[0][0] - p[2][0])) / D
			# 22, [(ay - by)^2 + (bx - ax)^2]
			M[8] = R * ((p[0][1] - p[1][1])*(p[0][1] - p[1][1]) + (p[1][0] - p[0][0])*(p[1][0] - p[0][0])) / D
			# 01, [(by - cy)(cy - ay) + (cx - bx)(ax - cx)]
			M[1] = R * ((p[1][1] - p[2][1])*(p[2][1] - p[0][1]) + (p[2][0] - p[1][0])*(p[0][0] - p[2][0])) / D
			# 12, [(cy - ay)(ay - by) + (ax - cx)(bx - ax)]
			M[5] = R * ((p[2][1] - p[0][1])*(p[0][1] - p[1][1]) + (p[0][0] - p[2][0])*(p[1][0] - p[0][0])) / D
			# 02, [(ay - by)(by - cy) + (bx - ax)(cx - bx)]
			M[2] = R * ((p[0][1] - p[1][1])*(p[1][1] - p[2][1]) + (p[1][0] - p[0][0])*(p[2][0] - p[1][0])) / D
			# Have it symmetric
			M[3] = M[1]
			M[6] = M[2]
			M[7] = M[5]
		else:
			print "Elem {} D = 0".format(element_index)
		
		return M
	
	# Creates the system matrix
	def create_system_m(self):
		# N points
		N = self.ew * self.eh
		# NxN matrix
		self.system_m = NZMatrix(N, N)
		# Ne elements
		Ne = self.get_num_elems()
		
		for k in range(0, Ne):
			eM = self.get_element_m(k)
			ids = self.get_point_ids(k)
			
			for i in range(0, 3):
				for j in range(0, 3):
					elem = self.system_m.get_elem(ids[i], ids[j]) + eM[i + j * 3]
					self.system_m.set_elem(ids[i], ids[j], elem)
					
		self.system_m.save_csv("system.csv")
		
	# Create a base for the simulation
	def create(self, w, h, scale):
		ESim.create(self, w, h, scale)
		
		# Scaling factor from pixels to element points
		self.es = [(self.ew - 1) / float(self.w), (self.eh - 1) / float(self.h)]
		
		# Note that in Python, 1D numpy arrays are a lot faster than 2D numpy arrays
		self.potential = (ctypes.c_float * (self.ew * self.eh))(0)
		self.temporary = (ctypes.c_float * (self.ew * self.eh))(0)
		self.b_vector = (ctypes.c_float * (self.ew * self.eh))(0)
		self.efield = (ctypes.c_float * (self.ew * self.eh))(0)
		self.metadata = (ctypes.c_uint * (self.ew * self.eh))(0)

		self.create_system_m()
		
		self.log.info("FEM Simulation field created")

	def set_algorithm(self, algorithm):
		self.algorithm = algorithm
		
		if algorithm != self.M_OVER_RELAXATION:
			self.alpha = 1.0
	
	# Fixes a point in the system matrix
	def fix_elem(self, point_index, potential):
		if (self.metadata[point_index] & self.MF_FIXED) != 0:
			self.potential[point_index] = potential
			self.b_vector[point_index] = potential
		else:
			# N points
			N = self.ew * self.eh
			for k in range(0, N):
				self.system_m.set_elem(point_index, k, 0)
			self.system_m.set_elem(point_index, point_index, 1)
			
			self.potential[point_index] = potential
			self.b_vector[point_index] = potential
			self.metadata[point_index] |= self.MF_FIXED
		
	def draw_point(self, x, y, potential):
		point_index = int(math.floor(x * self.es[0]) + math.floor(y * self.es[1]) * self.ew)
		self.fix_elem(point_index, potential)
	
	@QtCore.Slot(int, int, int, int, float)
	# Draw a rectangle on the simulation space
	def draw_rect(self, x0, y0, x1, y1, potential):
		if self.potential == None or self.metadata == None:
			return
		
		if potential < self.min_value:
			self.min_value = potential
		if potential > self.max_value:
			self.max_value = potential
		
		for y in range(y0, y1):
			for x in range(x0, x1):
				self.draw_point(x, y, potential)
		
		self.log.debug("FEM: Rect({}, {}, {}, {}, {} V)".format(x0, y0, x1, y1, potential))
	
	@QtCore.Slot(int, int, int, int, float)
	# Draw an ellipse on the simulation space
	def draw_ellipse(self, x0, y0, x1, y1, potential):
		if self.potential == None or self.metadata == None:
			return
		
		if potential < self.min_value:
			self.min_value = potential
		if potential > self.max_value:
			self.max_value = potential
		
		# http://stackoverflow.com/questions/10322341/simple-algorithm-for-drawing-filled-ellipse-in-c-c
		ox = (x0 + x1) / 2
		oy = (y0 + y1) / 2
		w = (x1 - x0) / 2
		h = (y1 - y0) / 2
		for y in range(-h, h + 1):
			for x in range(-w, w + 1):
				if (x * x * h * h + y * y * w * w <= h * h * w * w):
					self.draw_point(ox + x, oy + y, potential)
		
		self.log.debug("FEM: Ellipse({}, {}, {}, {}, {} V)".format(x0, y0, x1, y1, potential))
	
	def init_algorithm(self):
		self.efield_valid = False
		self.save_m_csv("potential.csv", self.potential, self.ew)
		
		self.log.info("FEM algorithm initialized")

	def run_algorithm(self):
		if self.algorithm == self.M_GAUSS_SEIDEL or self.algorithm == self.M_OVER_RELAXATION:
			self.m_gauss_seidel()
		elif self.algorithm == self.M_GAUSS:
			self.m_gauss()
		elif self.algorithm == self.M_CHOLESKY:
			self.m_cholesky()
	
	def end_algorithm(self):
		self.system_m.save_csv("system2.csv")
		# Convert the results into an image
		self.output_potential()
		#ESim.end_algorithm(self)
		
	def m_gauss(self):
		if self.ew == 0 or self.eh == 0:
			return
		
		# Have a C simulation math lib? Use it
		#if self.libsim_math != None and self.use_cmath:
		#	if self.coordsys == self.CS_CARTESIAN_2D:
		#		self.error_so_far = self.libsim_math.fgauss_seidel(self.potential, self.metadata, self.w, self.h, ctypes.c_float(self.alpha))
		#	elif self.coordsys == self.CS_CYLINDRICAL:
		#		self.error_so_far = self.libsim_math.fgauss_seidel_cyl(self.potential, self.metadata, self.w, self.h, ctypes.c_float(self.alpha))
		# No C simulation math lib? It'll be somewhat slower, then.
		#else:
		
		N = self.ew * self.eh
		
		# Cartesian or cylindrical coordinates on 2D plane
		if self.coordsys == self.CS_CARTESIAN_2D or self.coordsys == self.CS_CYLINDRICAL:
			# Might be a little bit faster when variables are created in advance?
			factor = 1.0
			elem_jj = 0.0
			elem_ij = 0.0
			elem_ii = 0.0
			elem_ik = 0.0
			elem_jk = 0.0
			
			# Solve the system matrix
			for j in range(0, N - 1):
				elem_jj = self.system_m.get_elem(j, j)
				if elem_jj != 0:
					for i in range(j + 1, N):
						elem_ij = self.system_m.get_elem(i, j)
						if elem_ij != 0:
							factor = elem_ij / elem_jj
							
							for k in range(0, N):
								elem_ik = self.system_m.get_elem(i, k)
								elem_jk = self.system_m.get_elem(j, k)
								self.system_m.set_elem(i, k, elem_ik - factor * elem_jk)
							
							self.b_vector[i] -= factor * self.b_vector[j]
			# Calculate the final potential values
			for i in range(N - 1, -1, -1):
				self.potential[i] = self.b_vector[i]
				for j in range(i + 1, N):
					self.potential[i] -= self.system_m.get_elem(i, j) * self.potential[j]
				elem_ii = self.system_m.get_elem(i, i)
				if elem_ii != 0:
					self.potential[i] /= elem_ii

			# Gauss algorithm does it all in 1 iteration, thus, error is as low as it gets
			self.error_so_far = 0

	def m_cholesky(self):
		if self.ew == 0 or self.eh == 0:
			return
		
		N = self.ew * self.eh
		
		if self.ch_triangle == None:
			self.ch_triangle = NZMatrix(N, N)
		
		diagonal = (ctypes.c_float * N)(0)
		elem_ik = 0
		elem_jk = 0
		elem_ji = 0
		
		# Cartesian or cylindrical coordinates on 2D plane
		if self.coordsys == self.CS_CARTESIAN_2D or self.coordsys == self.CS_CYLINDRICAL:
			for i in range(0, N):
				# Calculate diagonal elements
				diagonal[i] = self.system_m.get_elem(i, i)
				for k in range(0, i):
					elem_ik = self.ch_triangle.get_elem(k, i)#self.ch_triangle.get_elem(i, k)
					diagonal[i] -= diagonal[k] * elem_ik * elem_ik
				# Calculate triangle matrix
				for j in range(i + 1, N):
					elem_ji = self.system_m.get_elem(i, j)#self.system_m.get_elem(j, i)
					for k in range(0, i):
						elem_ik = self.ch_triangle.get_elem(i, k)
						elem_jk = self.ch_triangle.get_elem(j, k)
						elem_ji -= diagonal[k] * elem_ik * elem_jk
					self.ch_triangle.set_elem(j, i, elem_ji / diagonal[i])
				# Diagonal elements of the triangle matrix must be all 1
				self.ch_triangle.set_elem(i, i, 1)
			
			# Solve the subproblem
			for i in range(0, N):
				self.temporary[i] = self.b_vector[i]
				for k in range(0, i):
					self.temporary[i] -= self.temporary[k] * self.ch_triangle.get_elem(i, k)
			# Final potential values
			for i in range(N - 1, -1, -1):
				self.potential[i] = self.temporary[i] / diagonal[i]
				for k in range(i + 1, N):
					self.potential[i] -= self.potential[k] * self.ch_triangle.get_elem(k, i)

			# Use the maximum difference as an estimate for error
			self.error_so_far = 0
		
	def m_gauss_seidel(self):
		if self.ew == 0 or self.eh == 0:
			return
		# No alpha? Calculate it
		if self.alpha == None:
			w = self.ew - 2
			h = self.eh - 2
			self.alpha = 2 - math.pi * math.sqrt(2.0 / (h * h) + 2.0 / (w * w))
		
		# Have a C simulation math lib? Use it
		#if self.libsim_math != None and self.use_cmath:
		#	if self.coordsys == self.CS_CARTESIAN_2D:
		#		self.error_so_far = self.libsim_math.fgauss_seidel(self.potential, self.metadata, self.w, self.h, ctypes.c_float(self.alpha))
		#	elif self.coordsys == self.CS_CYLINDRICAL:
		#		self.error_so_far = self.libsim_math.fgauss_seidel_cyl(self.potential, self.metadata, self.w, self.h, ctypes.c_float(self.alpha))
		# No C simulation math lib? It'll be somewhat slower, then.
		#else:
		
		max_diff = 0
		N = self.ew * self.eh
		
		# Cartesian or cylindrical coordinates on 2D plane
		if self.coordsys == self.CS_CARTESIAN_2D or self.coordsys == self.CS_CYLINDRICAL:
			for i in range(0, N):
				self.temporary[i] = self.potential[i]
				self.potential[i] = self.b_vector[i]
				for j in range(0, N):
					if i != j:
						self.potential[i] -= self.system_m.get_elem(i, j) * self.potential[j]
				self.potential[i] = (1 - self.alpha) * self.temporary[i] + self.alpha * self.potential[i] / self.system_m.get_elem(i, i)
				max_diff = max(max_diff, self.potential[i] - self.temporary[i])

			# Gauss algorithm does it all in 1 iteration, thus, error is as low as it gets
			self.error_so_far = max_diff

	def m_efield(self):
		self.log.info("Calculating electric field")
		
		if self.libsim_math != None and self.use_cmath:
			self.libsim_math.felectric_field(self.potential, self.efield, self.ew, self.eh, ctypes.c_float(self.edensity), ctypes.c_bool(self.coordsys == self.CS_CYLINDRICAL))
		else:
			w = self.ew
			h = self.eh
			for y in range(0, self.eh):
				for x in range(0, self.ew):
					if x > 0 and x < w -1 and y > 0 and y < h - 1:
						dx = (self.potential[x + 1 + y * w] - self.potential[x - 1 + y * w]) * self.edensity / 2.0
						dy = (self.potential[x + (y + 1) * w] - self.potential[x + (y - 1) * w]) * self.edensity / 2.0
					else:
						# Single-sided derivative
						if x == 0:
							if self.coordsys == self.CS_CYLINDRICAL:
								dx = abs(self.potential[x + (y + 1) * w] - self.potential[x + (y - 1) * w]) * self.edensity / 2.0
							else:
								dx = (self.potential[x + 1 + y * w] - self.potential[x + y * w]) * self.edensity
						elif x == w - 1:
							dx = (self.potential[x + y * w] - self.potential[x - 1 + y * w]) * self.edensity
						if y == 0:
							dy = (self.potential[x + (y + 1) * w] - self.potential[x + y * w]) * self.edensity
						elif y == h - 1:
							dy = (self.potential[x + y * w] - self.potential[x + (y - 1) * w]) * self.edensity
					
					self.efield[x + y * w] = math.sqrt(dx * dx + dy * dy)
		
		# Find minimum and maximum values
		self.min_efvalue = min(self.efield)
		self.max_efvalue = max(self.efield)
		
		self.efield_valid = True
	
	# Convert it into an image
	def to_image(self, output_fname, data, min_value, max_value):
		self.log.info("Converting to image")
		
		if max_value - min_value != 0:
			for i in range(0, 256):
				self.image.setColor(i, QtGui.qRgb(i, i, i))
			
			N = self.get_num_elems()
			for i in range(0, N):
				pc = self.get_point_coords(i)
				pidx = self.get_point_ids(i)
				
				c = [0, 0, 0]
				for j in range(0, 3):
					# Normalize and convert potential into 8-bit values
					c[j] = int(round(255 * ((data[pidx[j]] - min_value) / (max_value - min_value)))) & 0xFF
					# Convert from point coordinates to image coordinates
					pc[j][0] = int(pc[j][0])
					pc[j][1] = int(pc[j][1])

				render_triangle_to_image(self.image, pc[0], pc[1], pc[2], c[0], c[1], c[2])
		
		self.log.info("Saving image")
		self.image.save(output_fname)
	
	@QtCore.Slot(bool)
	def output_efield(self, show=True):
		if not self.efield_valid:
			self.m_efield()
		
		if show:
			self.log.info("Converting electric field to an image")
			self.to_image("efield.png", self.efield, self.min_efvalue, self.max_efvalue)
			ESim.end_algorithm(self)
	
	@QtCore.Slot(bool)
	def output_potential(self, show=True):
		self.save_m_csv("potential2.csv", self.potential, self.ew)
		if show:
			self.log.info("Converting potential field to an image")
			self.to_image("potential.png", self.potential, self.min_value, self.max_value)
			ESim.end_algorithm(self)
			
	def save_sim(self, f):
		ESim.save_sim(self, f)
		f.write('sim.ew = {}\nsim.eh = {}\nsim.edensity = {}\n'.format(self.ew, self.eh, self.edensity))

	def get_value(self, x, y):
		potential = ESim.get_value(self, x, y) * (self.max_value - self.min_value) / 255.0 + self.min_value
		return "{} V".format(potential)
	