from PySide import QtGui, QtCore

from simulation import ESim

import ctypes
import numpy, scipy, scipy.special, scipy.linalg
import math

"""
Charge simulation
"""
class ESimC(ESim):
	Algorithms = ["Numpy"]
	
	def __init__(self):
		ESim.__init__(self)
		
		self.potential = None
		self.efield = None
		self.efield_valid = False
		self.edensity = 10.0
		self.epsilon0 = 8.854e-12
		self.m_k = None
		self.v_cp = None
		self.v_q = None
		
		self.min_value = 0
		self.max_value = 0
		self.min_efvalue = 0
		self.max_efvalue = 0
		
		self.point_charges = []
		self.line_charges = []
		self.circle_charges = []
		self.control_points = []
		
		self.algorithm = None
	
	# Create a base for the simulation
	def create(self, w, h, scale):
		ESim.create(self, w, h, scale)
		
		self.potential = (ctypes.c_float * (self.w * self.h))(0)
		self.efield = (ctypes.c_float * (self.w * self.h))(0)
		
		self.log.info("Charge Simulation field created")
		
	@QtCore.Slot(int, int, float)
	def create_point_charge(self, x, y, potential):
		if potential < self.min_value:
			self.min_value = potential
		if potential > self.max_value:
			self.max_value = potential
		
		self.point_charges.append([x, y, potential])
		self.log.debug("C: Point({}, {}, {} V)".format(x, y, potential))

	@QtCore.Slot(int, int, int, int, float)
	def create_line_charge(self, x0, y0, x1, y1, potential):
		if potential < self.min_value:
			self.min_value = potential
		if potential > self.max_value:
			self.max_value = potential
		
		self.line_charges.append([x0, y0, x1, y1, potential])
		self.log.debug("C: Line({}, {}, {}, {}, {} V)".format(x0, y0, x1, y1, potential))

	@QtCore.Slot(int, int, int, float)
	def create_circle_charge(self, x, y, w, potential):
		if potential < self.min_value:
			self.min_value = potential
		if potential > self.max_value:
			self.max_value = potential
		
		self.circle_charges.append([x, y, w, potential])
		self.log.debug("C: Circle({}, {}, {}, {} V)".format(x, y, w, potential))

	@QtCore.Slot(int, int, float)
	def create_control_point(self, x, y, potential):
		self.control_points.append([x, y, potential])
		self.log.debug("C: ControlPoint({}, {}, {} V)".format(x, y, potential))
	
	def init_algorithm(self):
		self.efield_valid = False
		self.log.info("Charge simulation algorithm initialized")

	def run_algorithm(self):
		self.m_calculate()
	
	def end_algorithm(self):
		# Convert the results into an image
		self.output_potential()
		#ESim.end_algorithm(self)
	
	def p_point_charge(self, x, y, pcid):
		xp = self.point_charges[pcid][0]
		yp = self.point_charges[pcid][1]
		xd2 = (x - xp)**2
		s1 = math.sqrt(xd2 + (y - yp)**2)
		s2 = math.sqrt(xd2 + (y + yp)**2)
		
		if s1 == 0 or s2 == 0:
			return 0
		
		return (1 / s1 - 1 / s2) / (4 * math.pi * self.epsilon0)
	
	def p_line_charge(self, x, y, lcid):
		xl0 = self.line_charges[lcid][0]
		yl0 = self.line_charges[lcid][1]
		xl1 = self.line_charges[lcid][2]
		yl1 = self.line_charges[lcid][3]
		d1 = (yl0 - y + math.sqrt(x**2 + (y - yl0)**2))
		d2 = (yl1 - y + math.sqrt(x**2 + (y + yl1)**2))
		
		if d1 == 0 or d2 == 0:
			return 0
		
		ln1 = math.log((yl1 - y + math.sqrt(x**2 + (y - yl1)**2)) / d1)
		ln2 = math.log((yl0 - y + math.sqrt(x**2 + (y + yl0)**2)) / d2)
		return (ln1 - ln2) / (4 * math.pi * self.epsilon0)
	
	def p_circle_charge(self, x, y, ccid):
		w = self.circle_charges[ccid][2]
		yp = self.circle_charges[ccid][1]
		
		xpw = (x + w)**2
		ymyp = (y - yp)**2
		ypyp = (y + yp)**2
		a = scipy.special.ellipk(4 * x * w / (xpw + ymyp)) / math.sqrt(xpw + ymyp)
		b = scipy.special.ellipk(4 * x * w / (xpw + ypyp)) / math.sqrt(xpw + ypyp)
		return (a - b) / (2 * (math.pi**2) * self.epsilon0)
	
	def calc_coeff_m(self):
		Nk = len(self.control_points)
		Np = len(self.point_charges)
		Nl = len(self.line_charges)
		Nc = len(self.circle_charges)
		
		if self.m_k == None:
			self.m_k = numpy.matrix(numpy.zeros(shape=(Nk, Nk), dtype='float32'))
		if self.v_cp == None:
			self.v_cp = numpy.zeros(shape=(Nk), dtype='float32')
		
		for i in range(0, Nk):
			cp = self.control_points[i]
			self.v_cp[i] = cp[2]
			# Add influence from point charges
			for j in range(0, len(self.point_charges)):
				self.m_k[i, j] = self.p_point_charge(cp[0], cp[1], j)
			# Add influence from line charges
			for j in range(0, len(self.line_charges)):
				self.m_k[i, j + Np] = self.p_line_charge(cp[0], cp[1], j)
			# Add influence from circle charges
			for j in range(0, len(self.circle_charges)):
				self.m_k[i, j + Np + Nl] = self.p_circle_charge(cp[0], cp[1], j)
		
		numpy.savetxt('m_k.csv', self.m_k, delimiter=',')
	
	def calc_u(self, x, y):
		Np = len(self.point_charges)
		Nl = len(self.line_charges)
		Nc = len(self.circle_charges)
		
		v_k = numpy.matrix(numpy.zeros(shape=(1, Np + Nl + Nc), dtype='float32'))
		
		# Add influence from point charges
		for i in range(0, len(self.point_charges)):
			v_k[0, i] = self.p_point_charge(x, y, i)
		# Add influence from line charges
		for i in range(0, len(self.line_charges)):
			v_k[0, Np + i] = self.p_line_charge(x, y, i)
		# Add influence from circle charges
		for i in range(0, len(self.circle_charges)):
			v_k[0, Np + Nl + i] = self.p_circle_charge(x, y, i)
		
		return v_k.dot(self.v_q)[0, 0]
	
	def m_calculate(self):
		if self.w == 0 or self.h == 0:
			return
		
		# Calculate the matrix of coefficients
		self.calc_coeff_m()
		
		max_diff = 0
		# Cartesian coordinates or Cylindrical coordinates on 2D plane
		if self.coordsys == self.CS_CARTESIAN_2D or self.coordsys == self.CS_CYLINDRICAL:
			Nk = len(self.control_points)
			
			numpy.savetxt('v_cp.csv', self.v_cp, delimiter=',')
			# Solve matrix equation Q = K.I * u
			# self.v_q = self.m_k.I.dot(self.v_cp).T
			# Thanks to Jaak Vaabel, who found http://www.johndcook.com/blog/2010/01/19/dont-invert-that-matrix/
			self.v_q = scipy.linalg.solve(self.m_k, self.v_cp)
			numpy.savetxt('v_q.csv', self.v_q, delimiter=',')
			
			# Calculate potential for all pixels
			for i in range(0, self.h, 1):
				for j in range(0, self.w, 1):
					self.potential[j + i * self.w] = self.calc_u(j, i)
			
			#self.save_m_csv("potential.csv", self.potential, self.w)
			
		# Use the maximum difference as an estimate for error
		self.error_so_far = max_diff
		
	def m_efield(self):
		self.log.info("Calculating electric field")
		
		if self.libsim_math != None and self.use_cmath:
			self.libsim_math.felectric_field(self.potential, self.efield, self.w, self.h, ctypes.c_float(self.edensity), ctypes.c_bool(self.coordsys == self.CS_CYLINDRICAL))
		else:
			w = self.w
			h = self.h
			for y in range(0, self.h):
				for x in range(0, self.w):
					if x > 0 and x < w -1 and y > 0 and y < h - 1:
						dx = (self.potential[x + 1 + y * w] - self.potential[x - 1 + y * w]) * self.edensity / 2.0
						dy = (self.potential[x + (y + 1) * w] - self.potential[x + (y - 1) * w]) * self.edensity / 2.0
					else:
						# Single-sided derivative
						if x == 0:
							if self.coordsys == self.CS_CYLINDRICAL:
								dx = abs(self.potential[x + (y + 1) * w] - self.potential[x + (y - 1) * w]) * self.edensity / 2.0
							else:
								dx = (self.potential[x + 1 + y * w] - self.potential[x + y * w]) * self.edensity
						elif x == w - 1:
							dx = (self.potential[x + y * w] - self.potential[x - 1 + y * w]) * self.edensity
						if y == 0:
							dy = (self.potential[x + (y + 1) * w] - self.potential[x + y * w]) * self.edensity
						elif y == h - 1:
							dy = (self.potential[x + y * w] - self.potential[x + (y - 1) * w]) * self.edensity
					
					self.efield[x + y * w] = math.sqrt(dx * dx + dy * dy)
		
		# Find minimum and maximum values
		self.min_efvalue = min(self.efield)
		self.max_efvalue = max(self.efield)
		
		self.efield_valid = True
	
	@QtCore.Slot()
	def output_efield(self):
		if not self.efield_valid:
			self.m_efield()
		
		self.log.info("Converting electric field to an image")
		
		# Convert it to an image and emit a "done" signal to display it
		self.to_image("efield.png", self.efield, self.min_efvalue, self.max_efvalue)
		ESim.end_algorithm(self)
	
	@QtCore.Slot()
	def output_potential(self):
		self.log.info("Converting potential field to an image")
		
		# Convert it to an image and emit a "done" signal to display it
		self.to_image("potential.png", self.potential, self.min_value, self.max_value)
		ESim.end_algorithm(self)

	def save_sim(self, f):
		ESim.save_sim(self, f)
		f.write('sim.edensity = {}\nsim.epsilon0 = {}\n'.format(self.edensity, self.epsilon0))

	def get_value(self, x, y):
		potential = ESim.get_value(self, x, y) * (self.max_value - self.min_value) / 255.0 + self.min_value
		return "{} V".format(potential)
	